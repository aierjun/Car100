package com.aierjun.car100.login;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aierjun.car100.R;
import com.aierjun.car100.init.MPermission;
import com.aierjun.car100.init.MyApplication;
import com.aierjun.car100.main.MainActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView login_login ,login_forget_password;
    private EditText login_user_name,login_user_password;
    private String TAG = "LoginActivity";
    private long onBackPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        MPermission.with(this)
                .setPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA,Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS)
                .requestPermission();
        findView();
        initView();
    }

    private void findView() {
        login_user_name = (EditText) findViewById(R.id.login_user_name);
        login_user_password = (EditText) findViewById(R.id.login_user_password);
        login_login = (TextView) findViewById(R.id.login_login);
        login_forget_password = (TextView) findViewById(R.id.login_forget_password);
    }

    private void initView() {
        login_login.setOnClickListener(this);
        login_forget_password.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_login:
                login();
                break;
            case R.id.login_forget_password:
                startActivity(new Intent(LoginActivity.this,ForgetPasswordActivity.class));
                break;
        }
    }

    public void login(){
        String userName = login_user_name.getText().toString();
        String userPassword = login_user_password.getText().toString();
        Log.d(TAG,"userName " + userName + " userPassword " + userPassword);
        //....
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - onBackPressedTime < 1000){
            MyApplication.appExit();
        }
        Toast.makeText(this,"再点击一次退出",Toast.LENGTH_SHORT).show();
        onBackPressedTime = System.currentTimeMillis();
    }
}
