package com.aierjun.car100.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aierjun.car100.R;
import com.sam.widget.headerBar.LeftImgTitleBar;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ani_aierJun on 2017/9/14.
 */

public class ForgetPasswordActivity extends Activity implements View.OnClickListener {
    private String TAG = "ForgetPasswordActivity";
    private EditText forget_phone_ed, forget_code_ed, forget_password_ed, forget_sure_password_ed;
    private TextView forget_get_code, forget_sure;
    private RelativeLayout forget_phone_relativeLayout;
    private boolean codeSel = false;
    private int time = 60;
    private String userPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        init();
        findView();
        initView();
    }

    private void findView() {
        forget_phone_ed = (EditText) findViewById(R.id.forget_phone_ed);
        forget_code_ed = (EditText) findViewById(R.id.forget_code_ed);
        forget_password_ed = (EditText) findViewById(R.id.forget_password_ed);
        forget_sure_password_ed = (EditText) findViewById(R.id.forget_sure_password_ed);
        forget_get_code = (TextView) findViewById(R.id.forget_get_code);
        forget_sure = (TextView) findViewById(R.id.forget_sure);
        forget_phone_relativeLayout = (RelativeLayout) findViewById(R.id.forget_phone_relativeLayout);
    }

    private void initView() {
        forget_get_code.setOnClickListener(this);
        forget_sure.setOnClickListener(this);
        if (userPhone != null && !userPhone.equals("") && userPhone.matches("^[1][3578]\\d{9}"))
            forget_phone_relativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forget_get_code:
                codeSel = true;
                forget_get_code.setSelected(true);
                codeTimer();
                break;
            case R.id.forget_sure:
                if (editTextVerift() != null)
                    Toast.makeText(this, editTextVerift(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private String editTextVerift() {
        String photo = forget_phone_ed.getText().toString();
        String code = forget_code_ed.getText().toString();
        String password = forget_password_ed.getText().toString();
        String passwordSure = forget_sure_password_ed.getText().toString();
        if (forget_phone_relativeLayout.getVisibility() != View.GONE){
            if (photo.length() != 11)
                return "手机号必须是11位";
            if (!photo.matches("^[1][3578]\\d{9}"))
                return "手机号是不合法的";
        }
        if (code.length() != 6)
            return "验证码必须是6位";
        if (password == null || password.equals("") || password.length() < 6)
            return "新密码不能为空且大于等于6位";
        if (!passwordSure.equals(password))
            return "两次密码不一致";
        return null;
    }

    private void codeTimer(){
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (codeSel == true){
                    time--;
                    handler.obtainMessage(1,time).sendToTarget();
                }
            }
        }, 1000, 1000);
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1){
                forget_get_code.setText("再次发送("+ time +"s)");
                if ((int)msg.obj == 0){
                    forget_get_code.setSelected(false);
                    codeSel = false;
                    forget_get_code.setText("获取验证码");
                }
            }
        }
    };

    private void init(){
        Intent intent = getIntent();
        userPhone = intent.getStringExtra("Phone");
    }
}
