package com.aierjun.car100.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;

/**
 * Created by Ani_aierJun on 2017/9/8.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class PickerScrollView extends View {
    private String[] data = null;//数据数组
    private int chooseSize = 20; //选择的字的大小
    private float mViewHeight;
    private float mViewWidth;
    private Paint paint;
    private Paint paintOther;
    private float padding = 30;//文字上下padding
    private int h;

    private int indexChoose = 0;//选中的下角标位
    private float pointsChoose;
    private List<Float> listPoints;

    private float pointsMoveItem;


    public PickerScrollView(Context context) {
        this(context, null);
    }

    public PickerScrollView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PickerScrollView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setData(String[] data) {
        this.data = data;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint = new Paint();
        paint.setTextSize(chooseSize);
        paint.setAlpha(255);
        Rect rect = new Rect();
        paint.getTextBounds(data[indexChoose], 0, data[indexChoose].length(), rect);
        h = rect.height();

        pointsChoose = mViewHeight / 2 + h + padding + moveY;

        canvas.drawLine(0, mViewHeight / 2, mViewWidth, mViewHeight / 2, paint);
        canvas.drawText(data[indexChoose], mViewWidth / 2, pointsChoose, paint);
        canvas.drawLine(0, mViewHeight / 2 + h + padding * 2, mViewWidth, mViewHeight / 2 + h + padding * 2, paint);
        onDrawOtherTop(canvas);
        onDrawOtherBottom(canvas);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mViewHeight = MeasureSpec.getSize(heightMeasureSpec);
        mViewWidth = MeasureSpec.getSize(widthMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    public void onDrawOtherBottom(Canvas canvas) {
        for (int i = 0; i < (data.length) - indexChoose - 1; i++) {
            canvas.drawText(data[indexChoose + i + 1], mViewWidth / 2, pointsChoose + (h + padding * 2) * (i + 1), paint);
        }

    }

    public void onDrawOtherTop(Canvas canvas) {
        for (int i = 0; i < indexChoose; i++) {
            canvas.drawText(data[indexChoose - i - 1], mViewWidth / 2, pointsChoose - (h + padding * 2) * (i + 1), paint);
        }
    }

    private float startY = 0;
    private float moveY = 0;  //两点间的距离

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case ACTION_DOWN:
                startY = event.getY();
                break;
            case ACTION_UP:
                getIndex();
                addPointsList(indexChoose);
                drawItem();
                break;
            case ACTION_MOVE:
                moveY = (event.getY() - startY) / 2.0f;
                invalidate();
                break;
        }
        return true;
    }

    public int getIndex() {
        pointsMoveItem = moveY / (h + padding * 2);
        if (moveY < 0) {
            float f = moveY / (h + padding * 2) - 0.5f;
            indexChoose = indexChoose + (int) Math.abs(f);
            Log.d("Jun ", indexChoose + "................" + f);
            if (indexChoose >= data.length)
                indexChoose = data.length - 1;
        }
        if (moveY >= 0) {
            float f = moveY / (h + padding * 2) + 0.5f;
            indexChoose = indexChoose - (int) Math.abs(f);
            Log.d("Jun ", indexChoose + "................" + f);
            if (indexChoose < 0)
                indexChoose = 0;
            if (indexChoose >= data.length)
                indexChoose = data.length - 1;
        }
        Log.d("Jun ", indexChoose + "................");
        return indexChoose;
    }

    public void drawItem() {
//        if (pointsMoveItem < 0){
//            float f = pointsChoose - listPoints.get(indexChoose);
//            Log.d("Jun",f + "............" + pointsChoose + ".................." + listPoints.get(indexChoose));
//        }
    }

    public List<Float> addPointsList(int index) {
        if (listPoints == null)
            listPoints = new ArrayList<>();
        listPoints.clear();
        for (int i = 0; i < index; i++)
            listPoints.add((mViewHeight / 2 + h + padding) - (padding + h + padding) * (index - i));
        for (int i = index; i < data.length; i++)
            listPoints.add((mViewHeight / 2 + h + padding) + (padding + h + padding) * i);
        Log.d("Jun ", " addPointsList " + listPoints.size() + " index " + index + "  listPoints[] " + listPoints.get(index) + "......... " );
        return listPoints;
    }
}
