package com.aierjun.car100.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.TextView;

import com.aierjun.car100.R;


/**
 * Created by aierJun on 2017/1/16.
 */
public abstract class MyDelWeightDialog extends Dialog {
    private TextView mes;
    private TextView left;
    private TextView right;
    public MyDelWeightDialog(Context context, View view) {
        super(context, R.style.style_dialog);
        setContentView(view);
    }
    public MyDelWeightDialog(Context context, @LayoutRes int view) {
        super(context,R.style.style_dialog);
        setContentView(view);
    }
    public MyDelWeightDialog(Context context) {
        super(context,R.style.style_dialog);
        View view= View.inflate(context,R.layout.activity_dialog_del,null);
        setContentView(view);
        mes= (TextView) view.findViewById(R.id.del_dialog_mes);
        left= (TextView) view.findViewById(R.id.sel_sure);
        right= (TextView) view.findViewById(R.id.sel_quire);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLeft(left);
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkRight(right);
            }
        });
    }

    protected abstract void checkLeft(TextView textView);

    protected abstract void checkRight(TextView textView);


    public void setDelMes(String string){
        if (mes!=null)
            mes.setText(string);
    }
    public void setLeftMes(String string){
        if (left!=null)
            left.setText(string);
    }
    public void setRightMes(String string){
        if (right!=null)
            right.setText(string);
    }
}
