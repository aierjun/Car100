package com.aierjun.car100.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.aierjun.car100.R;


/**
 * Created by Ani_aierJun on 2017/9/8.
 */

public class ImageChangeDialog extends Dialog {
    private int tag = 0;
    private TextView cream;
    private TextView photo;
    private TextView confirm;
    private OnClickLister onClickListers;

    public ImageChangeDialog(Context context) {
        super(context, R.style.style_dialog);
        View view = View.inflate(context, R.layout.upload_dialog,null);
        setContentView(view);
        cream = (TextView) view.findViewById(R.id.cream_btn);
        photo = (TextView) view.findViewById(R.id.photo_btn);
        confirm = (TextView) view.findViewById(R.id.confirm_btn);
        return;
    }

    public ImageChangeDialog showGravity(int gravity) {
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(gravity); //Gravity.BOTTOM
        return this;
    }

    public interface OnClickLister{
        void CLickCream(View view);

        void ClickPhoto(View view);

        void ClickCancal(View view);
    }

    public ImageChangeDialog setOnClickLister(final OnClickLister onClickLister){
        if (onClickLister != null){
            cream.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    onClickLister.CLickCream(v);
                }
            });
            photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    onClickLister.ClickPhoto(v);
                }
            });
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    onClickLister.ClickCancal(v);
                }
            });
        }
        return this;
    }
}

