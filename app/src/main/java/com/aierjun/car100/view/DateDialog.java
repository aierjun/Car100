package com.aierjun.car100.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;


import com.aierjun.car100.R;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by Ani_aierJun on 2017/9/8.
 */

public class DateDialog extends Dialog {
    private DatePicker datePicker;
    private CallBackDate callBackDate;
    private TextView leftText;
    private TextView rightText;

    public DateDialog(@NonNull final Context context) {
        super(context, R.style.style_dialog);
        View view = View.inflate(context, R.layout.activity_dialog_date, null);
        setContentView(view);
        datePicker = (DatePicker) view.findViewById(R.id.datePcker);
        leftText = (TextView) view.findViewById(R.id.leftText);
        rightText = (TextView) view.findViewById(R.id.rightText);
        datePicker.init(2016, 11, 10, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat format = new SimpleDateFormat(
                        "yyyy/MM/dd");
//                Toast.makeText(context,
//                        format.format(calendar.getTime()), Toast.LENGTH_SHORT)
//                        .show();
//                int year_index = getYearPosition();
//                for (int i = 0; i< 3 ;i++){
//                    if (year_index == i){
//                       NumberPicker v = (NumberPicker) ((ViewGroup)((ViewGroup)datePicker.getChildAt(0)).getChildAt(0)).getChildAt(i);
//                        v.getChildAt(0).setVisibility(View.GONE);
//                        Log.d("Jun" ,v.toString());
//                    }
//                }

                if (callBackDate != null){
                    callBackDate.callBackDateString(format.format(calendar.getTime()));
                }
            }
        });
        datePicker.setMaxDate(System.currentTimeMillis());
    }

    public interface  CallBackDate{
        void callBackDateString(String date);

        void clickLeft(View view);

        void clickRight(View view);
    }

    public void setCallBackDate(final CallBackDate callBackDate){
        this.callBackDate= callBackDate;
        if (callBackDate != null){
            leftText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBackDate.clickLeft(v);
                }
            });
            rightText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBackDate.clickRight(v);
                }
            });
        }
    }

    public DateDialog showGravity(int gravity) {
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(gravity); //Gravity.BOTTOM
        return this;
    }

    private int getYearPosition() {
        char[] order = DateFormat.getDateFormatOrder(getContext());
        final int spinnerCount = order.length;
        for (int i = 0 ; i< spinnerCount;i++){
            switch (order[i]){
                case 'd':
                    break;
                case 'M':
                    break;
                case 'y':
                    return i;
                default:
                    throw new IllegalArgumentException(Arrays.toString(order));
            }
        }
        return 0;
    }
}
