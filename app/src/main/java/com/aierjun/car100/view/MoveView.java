package com.aierjun.car100.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ani_aierJun on 2017/9/5.
 */

public class MoveView extends View {
    private boolean isLongClickModule = false;
    private float old_X = 0;
    private float old_Y = 0;
    private float new_X = 0;
    private float new_Y = 0;
    private int right = -1;
    private int bottom = -1;
    private  int screenWidth =0;
    private int screenHeight =0;

    private boolean isMoveModule = false;
    private boolean change = false;

    public MoveView(Context context) {
        this(context,null);
    }

    public MoveView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MoveView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null); //关闭硬件加速，解决拖动花屏bug
        WindowManager windowManager = ((Activity)(context)).getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        screenWidth = display.getWidth();
        screenHeight = display.getHeight();
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                Log.d("Jun","ACTION_DOWN");
                old_X = event.getX();
                old_Y = event.getY();
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (!isMoveModule){
                            isLongClickModule = true;
                            change = true;
                        }
                    }
                }, 1000);
                break;
            case MotionEvent.ACTION_MOVE:
                isMoveModule = true;
                if (isLongClickModule == true){
                    Log.d("Jun","ACTION_MOVE true");
                    //长按2秒后移动
                    new_X = event.getX();
                    new_Y = event.getY();
                    onMove();
                    requestLayout();
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                Log.d("Jun","ACTION_UP");
                isLongClickModule = false;
                isMoveModule = false;
                change = false;
                break;
        }
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.d("Jun","right " + right + " changed " + changed);
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d("Jun","draw");
        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d("Jun","onMeasure" + " widthMeasureSpec " + MeasureSpec.getSize(widthMeasureSpec) + " heightMeasureSpec " + MeasureSpec.getSize(heightMeasureSpec));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void onMove(){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
        Log.d("Jun", " leftMargin " + layoutParams.leftMargin + " rightMargin " + getRight());
        layoutParams.leftMargin += (int) (new_X - old_X);
        layoutParams.topMargin += (int) (new_Y - old_Y);
//        if (layoutParams.leftMargin < 0)
//            layoutParams.leftMargin =0;
//        if (layoutParams.topMargin < 0)
//            layoutParams.topMargin =0;
        MoveView.this.setLayoutParams(layoutParams);

    }
}
