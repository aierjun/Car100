package com.aierjun.car100.view;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;


import com.aierjun.car100.R;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/8.
 */

public class PickerViewDialog extends Dialog implements PickerView.onSelectListener {
    private int tag = 0;
    private TextView left;
    private TextView right;
    private OnClickLister onClickListers;
    private PickerView pickerView_1, pickerView_2;
    private String text_1;
    private List<String> list_1, list_22;
    private PickerView.onSelectListener onSelectListener_1;
    private List<List<String>> list_2;
    private PickerView.onSelectListener onSelectListener_2;
    private int TOB_1, TOB_2;

    public PickerViewDialog(Context context, int num) {
        super(context, R.style.style_dialog);
        View view = null;
        if (num == 2) {
            view = View.inflate(context, R.layout.activity_dialog_pickerview, null);
        } else {
            view = View.inflate(context, R.layout.activity_dialog_pickerview_one, null);
        }
        setContentView(view);
        pickerView_1 = (PickerView) view.findViewById(R.id.pickerView_1);
        TOB_1 = pickerView_1.getTOB();
        pickerView_2 = (PickerView) view.findViewById(R.id.pickerView_2);
        if (pickerView_2 != null){
            TOB_2 = pickerView_2.getTOB();
        }

        left = (TextView) view.findViewById(R.id.sel_sure);
        right = (TextView) view.findViewById(R.id.sel_quire);
        return;
    }

    public PickerViewDialog showGravity(int gravity) {
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(gravity); //Gravity.BOTTOM
        return this;
    }

    public PickerViewDialog setData(List<String> list_1, PickerView.onSelectListener onSelectListener_1) {
        this.list_1 = list_1;
        this.onSelectListener_1 = onSelectListener_1;
        pickerView_1.setData(list_1);
        pickerView_1.setOnSelectListener(onSelectListener_1);
        return this;
    }

    public PickerViewDialog setData2(List<String> list_2, PickerView.onSelectListener onSelectListener_2) {
        this.list_22 = list_2;
        this.onSelectListener_2 = onSelectListener_2;
        if (pickerView_2 != null) {
            pickerView_2.setData(list_2);
            pickerView_2.setOnSelectListener(onSelectListener_2);
        }
        return this;
    }

    public PickerViewDialog setDataList(List<String> list_1, PickerView.onSelectListener onSelectListener_1, List<List<String>> list_2, PickerView.onSelectListener onSelectListener_2) {
        this.list_1 = list_1;
        this.onSelectListener_1 = onSelectListener_1;
        this.list_2 = list_2;
        this.onSelectListener_2 = onSelectListener_2;
        pickerView_1.setData(list_1);
        pickerView_1.setOnSelectListener(this);
        if (pickerView_2 != null)
            pickerView_2.setOnSelectListener(onSelectListener_2);

        return this;
    }

    public PickerViewDialog setAgain(boolean again_1, boolean again_2) {
        pickerView_1.setAgain(again_1);
        if (pickerView_2 != null)
            pickerView_2.setAgain(again_2);
        return this;
    }

    @Override
    public void onSelect(String text) {
        text_1 = text;
        Log.d("Jun", text_1 + "......................");
        if (text_1 == null)
            text_1 = list_1.get(0);
        if (onSelectListener_1 != null)
            onSelectListener_1.onSelect(text_1);
        for (int i = 0; i < list_1.size(); i++) {
            if (text_1.equals(list_1.get(i))) {
                if (TOB_1 == 1) {
                    List<String> tail = list_2.get(list_2.size() - 1);
                    list_2.remove(list_2.size() - 1);
                    list_2.add(0, tail);
                } else {
                    List<String> head = list_2.get(0);
                    list_2.remove(0);
                    list_2.add(head);
                }
                if (pickerView_2 != null) {
                    pickerView_2.setData(list_2.get(i));
                    pickerView_2.setOnSelectListener(onSelectListener_2);
                }

            }
        }
    }

    public interface OnClickLister {
        void CLickLeft(View view);

        void ClickRight(View view);
    }

    public PickerViewDialog setOnClickLister(final OnClickLister onClickLister) {
        if (onClickLister != null) {
            left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    onClickLister.CLickLeft(v);
                }
            });
            right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    onClickLister.ClickRight(v);
                }
            });
        }
        return this;
    }
}

