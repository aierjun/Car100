package com.aierjun.car100.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.utils.PDUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 未考虑时间到了自动失败
 * Created by Ani_aierJun on 2017/9/14.
 */

public class RefreshListView extends ListView {
    private String TAG = "RefreshListView";
    private Context mContext;
    private int headerViewHeight;
    private int footerViewHeight;
    private View headerView;
    private View footerView;
    private float downY;
    private float moveY;
    private int TOB = 1;
    private Timer timer = new Timer();
    private int s = 1;
    private int failTime = 10;
    private boolean requet = false;

    private static final int TAP_TO_REFRESH = 1;     // 初始状态

    private static final int PULL_TO_REFRESH = 2;    //拉动刷新

    private static final int RELEASE_TO_REFRESH = 3;  //释放刷新

    private static final int REFRESHING = 4;    //正在刷新

    private int state = TAP_TO_REFRESH;
    private TextView topTextView;
    private TextView bottonTextView;

    private CallBack callBack;


    public RefreshListView(Context context) {
        this(context, null);
    }

    public RefreshListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RefreshListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initHeaderView();
        initFooterView();
        state = TAP_TO_REFRESH;
    }

    public RefreshListView getRefreshListView() {
        return this;
    }

    public void initHeaderView() {
        headerView = View.inflate(mContext, R.layout.listview_headerview, null);
        headerView.measure(0, 0);
        headerViewHeight = headerView.getMeasuredHeight();
        headerView.setPadding(0, -headerViewHeight, 0, 0);
        Log.d(TAG, headerViewHeight + "");
        this.addHeaderView(headerView);
        topTextView = ((TextView) ((LinearLayout) headerView).getChildAt(0));
    }

    public void initFooterView() {
        footerView = View.inflate(mContext, R.layout.listview_footerview, null);
        footerView.measure(0, 0);
        footerViewHeight = footerView.getMeasuredHeight();
        footerView.setPadding(0, 0, 0, -footerViewHeight);
        this.addFooterView(footerView);
        bottonTextView = ((TextView) ((LinearLayout) footerView).getChildAt(0));
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downY = ev.getY();
                requet = false;
                break;
            case MotionEvent.ACTION_UP:
                if (TOB == 0) {
                    break;
                } else if (TOB == 1) {
                    switch (state){
                        case TAP_TO_REFRESH: //初始状态

                            break;
                        case PULL_TO_REFRESH: //拉动刷新
                            headerView.setPadding(0, -headerViewHeight, 0, 0);
                            state = TAP_TO_REFRESH;
                            break;
                        case RELEASE_TO_REFRESH://释放刷新
                            headerView.setPadding(0, 0, 0, 0);
                            state = REFRESHING;
                            topTextView.setText("正在刷新");
                            if (callBack != null)
                                callBack.callBackTop();
                            break;
                        case REFRESHING://正在刷新

                            break;
                    }
                } else if (TOB == -1) {
                    switch (state){
                        case TAP_TO_REFRESH: //初始状态

                            break;
                        case PULL_TO_REFRESH: //拉动刷新
                            footerView.setPadding(0, 0, 0, -footerViewHeight);
                            state = TAP_TO_REFRESH;
                            break;
                        case RELEASE_TO_REFRESH://释放刷新
                            footerView.setPadding(0, 0, 0, 0);
                            state = REFRESHING;
                            bottonTextView.setText("正在加载");
                            if (callBack != null)
                                callBack.callBackBotton();
                            break;
                        case REFRESHING://正在刷新

                            break;
                    }
                }

                break;
            case MotionEvent.ACTION_MOVE:
                moveY = ev.getY() - downY;
                if (getChildCount() == 0) return super.onTouchEvent(ev);
                if (this.getChildAt(0).getTop() == 0) {
                    headerView.setPadding(0, -headerViewHeight + (int) (moveY / 3), 0, 0);
                    TOB = 1;
                    if (headerView.getPaddingTop() >= 0) {
                        state = RELEASE_TO_REFRESH;
                        topTextView.setText("释放刷新");
                    } else {
                        state = PULL_TO_REFRESH;
                        topTextView.setText("下拉刷新");
                    }
                } else if (this.getChildAt(this.getChildCount() - 1).getBottom() == this.getHeight()) {
                    footerView.setPadding(0, 0, 0, -headerViewHeight - (int) (moveY / 3));
                    TOB = -1;
                    if (footerView.getPaddingBottom() >= 0) {
                        state = RELEASE_TO_REFRESH;
                        bottonTextView.setText("释放加载");
                    } else {
                        state = PULL_TO_REFRESH;
                        bottonTextView.setText("上拉加载");
                    }
                }
                break;
        }
        return super.onTouchEvent(ev);
    }

    public interface CallBack {
        void callBackTop();
        void callBackBotton();
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    public void stopTOPLoading(boolean loadingSuccess) {
        if (requet == true)
            return;
        if (loadingSuccess)
            topTextView.setText("刷新成功");
        else
            topTextView.setText("刷新失败");
        topTextView.postDelayed(new Runnable() {
            @Override
            public void run() {
                requet = true;
                headerView.setPadding(0, -headerViewHeight, 0, 0);
                state = TAP_TO_REFRESH;
                TOB = 0;
            }
        }, 1000);
    }

    public void stopBottonLoading(boolean loadingSuccess) {
        if (loadingSuccess)
            bottonTextView.setText("加载成功");
        else
            bottonTextView.setText("加载失败");
        topTextView.postDelayed(new Runnable() {
            @Override
            public void run() {
                requet = true;
                footerView.setPadding(0, 0, 0, -footerViewHeight);
                state = TAP_TO_REFRESH;
                TOB = 0;
            }
        }, 1000);
    }
}
