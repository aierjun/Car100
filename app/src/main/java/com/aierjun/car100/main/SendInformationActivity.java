package com.aierjun.car100.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.liang.scancode.CommonScanActivity;

/**
 * Created by Ani_aierJun on 2017/9/19.
 */
/*
* 寄送资料
*
* */

public class SendInformationActivity extends BaseActivity {
    private String TAG = "SendInformationActivity";
    private TextView sendinformation_text;
    private EditText sendinformation_edit;
    private ImageView sendinformation_image;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendinformation);
        selBackImg();
        selCommitText();
        findView();
        initView();

    }

    private void findView() {
        sendinformation_text = (TextView) findViewById(R.id.sendinformation_text);
        sendinformation_edit = (EditText) findViewById(R.id.sendinformation_edit);
        sendinformation_image = (ImageView) findViewById(R.id.sendinformation_image);
    }

    private void initView() {
        sendinformation_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SendInformationActivity.this, CommonScanActivity.class),123);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null || data.equals("")) return;
        if (requestCode == 123 && resultCode == 123)
            Log.d(TAG, data.getStringExtra("ID"));
        sendinformation_text.setText(data.getStringExtra("ID"));
        super.onActivityResult(requestCode, resultCode, data);
    }
}
