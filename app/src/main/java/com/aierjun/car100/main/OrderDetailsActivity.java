package com.aierjun.car100.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.adapter.AfterLoaningInformationAdapter;
import com.aierjun.car100.adapter.BasicInformationAdapter;
import com.aierjun.car100.adapter.BuyCarUserInformationAdapter;
import com.aierjun.car100.adapter.CarInformationAdapter;
import com.aierjun.car100.adapter.CheckResultAdapter;
import com.aierjun.car100.adapter.GuaranteeInformationAdapter;
import com.aierjun.car100.adapter.OverdueInformationAdapter;
import com.aierjun.car100.adapter.SpouseInformationAdapter;
import com.aierjun.car100.entity.AfterLoaningInformationEntity;
import com.aierjun.car100.entity.BasicInformationEntity;
import com.aierjun.car100.entity.BuyCarUserInformationEntity;
import com.aierjun.car100.entity.CarInformationEntity;
import com.aierjun.car100.entity.CheckResultEntity;
import com.aierjun.car100.entity.GuaranteeInformationEntity;
import com.aierjun.car100.entity.OverdueInformationEntity;
import com.aierjun.car100.entity.SpouseInformationEntity;
import com.aierjun.car100.widget.RefreshListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class OrderDetailsActivity extends BaseActivity implements View.OnClickListener {
    private String TAG = "OrderDetailsActivity";
    private TextView orderdetails_btn_1, orderdetails_btn_2, orderdetails_btn_3, orderdetails_btn_4, orderdetails_btn_5, orderdetails_btn_6, orderdetails_btn_7, orderdetails_btn_8, orderdetails_btn_now;
    private RefreshListView orderdetails_listview;
    private LinearLayout overdue_layout;
    private TextView overdue_text,reminders_text;
    private List<BasicInformationEntity> basicInformationEntityList = new ArrayList<>();
    private List<BuyCarUserInformationEntity> buyCarUserInformationEntityList = new ArrayList<>();
    private List<SpouseInformationEntity> spouseInformationEntityList = new ArrayList<>();
    private List<GuaranteeInformationEntity> guaranteeInformationEntityList = new ArrayList<>();
    private List<CarInformationEntity> carInformationEntityList = new ArrayList<>();
    private List<CheckResultEntity> checkResultEntityList = new ArrayList<>();
    private List<OverdueInformationEntity> overdueInformationEntityList = new ArrayList<>();
    private List<AfterLoaningInformationEntity> afterLoaningInformationEntityList = new ArrayList<>();
    private int tagBtn = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        initData();
        findView();
        initView();
        selBackImg();
    }

    public void initData() {
        Intent intent = getIntent();
        intent.getStringExtra("");
    }

    private void findView() {
        orderdetails_btn_1 = (TextView) findViewById(R.id.orderdetails_btn_1);
        orderdetails_btn_2 = (TextView) findViewById(R.id.orderdetails_btn_2);
        orderdetails_btn_3 = (TextView) findViewById(R.id.orderdetails_btn_3);
        orderdetails_btn_4 = (TextView) findViewById(R.id.orderdetails_btn_4);
        orderdetails_btn_5 = (TextView) findViewById(R.id.orderdetails_btn_5);
        orderdetails_btn_6 = (TextView) findViewById(R.id.orderdetails_btn_6);
        orderdetails_btn_7 = (TextView) findViewById(R.id.orderdetails_btn_7);
        orderdetails_btn_8 = (TextView) findViewById(R.id.orderdetails_btn_8);
        orderdetails_listview = (RefreshListView) findViewById(R.id.orderdetails_listview);
        overdue_layout = (LinearLayout) findViewById(R.id.overdue_layout);
        overdue_text = (TextView) findViewById(R.id.overdue_text);
        reminders_text = (TextView) findViewById(R.id.reminders_text);
    }

    private void initView() {
        orderdetails_btn_1.setTag(1);
        orderdetails_btn_2.setTag(2);
        orderdetails_btn_3.setTag(3);
        orderdetails_btn_4.setTag(4);
        orderdetails_btn_5.setTag(5);
        orderdetails_btn_6.setTag(6);
        orderdetails_btn_7.setTag(7);
        orderdetails_btn_8.setTag(8);
        orderdetails_btn_1.setOnClickListener(this);
        orderdetails_btn_2.setOnClickListener(this);
        orderdetails_btn_3.setOnClickListener(this);
        orderdetails_btn_4.setOnClickListener(this);
        orderdetails_btn_5.setOnClickListener(this);
        orderdetails_btn_6.setOnClickListener(this);
        orderdetails_btn_7.setOnClickListener(this);
        orderdetails_btn_8.setOnClickListener(this);
        orderdetails_btn_now = orderdetails_btn_1;
        orderdetails_btn_1.setSelected(true);
        setAdapterDateList(1);
        overdue_text.setSelected(true);
        overdue_text.setOnClickListener(this);
        reminders_text.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.orderdetails_btn_1:
                selBtnShow(v);
                break;
            case R.id.orderdetails_btn_2:
                selBtnShow(v);
                break;
            case R.id.orderdetails_btn_3:
                selBtnShow(v);
                break;
            case R.id.orderdetails_btn_4:
                selBtnShow(v);
                break;
            case R.id.orderdetails_btn_5:
                selBtnShow(v);
                break;
            case R.id.orderdetails_btn_6:
                selBtnShow(v);
                break;
            case R.id.orderdetails_btn_7:
                selBtnShow(v);
                break;
            case R.id.orderdetails_btn_8:
                selBtnShow(v);
                break;
            case R.id.overdue_text:
                overdue_text.setSelected(true);
                reminders_text.setSelected(false);
                break;
            case R.id.reminders_text:
                overdue_text.setSelected(false);
                reminders_text.setSelected(true);
                break;
        }
    }

    public void selBtnShow(View v) {
        overdue_layout.setVisibility(View.GONE);
        orderdetails_btn_now.setSelected(false);
        v.setSelected(true);
        orderdetails_btn_now = (TextView) v;
        tagBtn = (int) v.getTag();
        Log.d(TAG, tagBtn + "");
        setAdapterDateList(tagBtn);
        if (tagBtn == 7)
            overdue_layout.setVisibility(View.VISIBLE);
    }

    public void setAdapterDateList(int tag) {
        switch (tag) {
            case 1:
                test1();
                BasicInformationAdapter basicInformationAdapter = new BasicInformationAdapter(this, basicInformationEntityList);
                orderdetails_listview.setAdapter(basicInformationAdapter);
                break;
            case 2:
                test2();
                BuyCarUserInformationAdapter buyCarUserInformationAdapter = new BuyCarUserInformationAdapter(this, buyCarUserInformationEntityList);
                orderdetails_listview.setAdapter(buyCarUserInformationAdapter);
                break;
            case 3:
              test3();
                SpouseInformationAdapter spouseInformationAdapter = new SpouseInformationAdapter(this,spouseInformationEntityList);
                orderdetails_listview.setAdapter(spouseInformationAdapter);
                break;
            case 4:
                //...
                GuaranteeInformationAdapter guaranteeInformationAdapter = new GuaranteeInformationAdapter(this,guaranteeInformationEntityList);
                orderdetails_listview.setAdapter(guaranteeInformationAdapter);
                break;
            case 5:
                test5();
                CarInformationAdapter carInformationAdapter = new CarInformationAdapter(this,carInformationEntityList);
                orderdetails_listview.setAdapter(carInformationAdapter);
                break;
            case 6:
                test6();
                CheckResultAdapter checkResultAdapter = new CheckResultAdapter(this,checkResultEntityList);
                orderdetails_listview.setAdapter(checkResultAdapter);
                break;
            case 7:
                //...
                OverdueInformationAdapter overdueInformationAdapter = new OverdueInformationAdapter(this,overdueInformationEntityList);
                orderdetails_listview.setAdapter(overdueInformationAdapter);
                break;
            case 8:
                test8();
                AfterLoaningInformationAdapter afterLoaningInformationAdapter = new AfterLoaningInformationAdapter(this,afterLoaningInformationEntityList);
                orderdetails_listview.setAdapter(afterLoaningInformationAdapter);
                break;
        }
    }

    private void test1() {
        basicInformationEntityList.clear();
        for (int i = 0; i < 10; i++) {
            BasicInformationEntity basicInformationEntity = new BasicInformationEntity();
            basicInformationEntity.setUserName("aierjun" + i);
            basicInformationEntity.setUserNumber(i + "");
            basicInformationEntity.setSignOperator("非签约营销商");
            basicInformationEntity.setOperator("个人散单");
            basicInformationEntityList.add(basicInformationEntity);
        }
    }

    private void test2() {
        buyCarUserInformationEntityList.clear();
        for (int i = 0; i < 1; i++) {
            BuyCarUserInformationEntity buyCarUserInformationEntity = new BuyCarUserInformationEntity();
            buyCarUserInformationEntity.setName("aierjun" + i);
            buyCarUserInformationEntity.setSex("男" + i);
            buyCarUserInformationEntity.setAge("" + i);
            buyCarUserInformationEntity.setCardNumber("420528" + i);
            buyCarUserInformationEntity.setCardAddress("djslajd" + i);
            buyCarUserInformationEntity.setEducational("大专" + i);
            buyCarUserInformationEntity.setCensusRegister("jdsljalf" + i);
            buyCarUserInformationEntity.setPhoneNumber("1785886115" + i);
            buyCarUserInformationEntity.setMarry("否" + i);
            buyCarUserInformationEntity.setAddressNow("kkkkkk" + i);
            buyCarUserInformationEntity.setTelPhone("0717-1231346" + i);
            buyCarUserInformationEntity.setHomeAddress("nnnn" + i);
            buyCarUserInformationEntity.setHomeRightAddress("lllll" + i);
            buyCarUserInformationEntity.setHomeRightUserName("aiersdjun" + i);
            buyCarUserInformationEntity.setBugCarRelation("兄弟" + i);
            buyCarUserInformationEntity.setHomeType("iiiii" + i);
            buyCarUserInformationEntity.setHomeSize(i);
            buyCarUserInformationEntity.setHomeMoney(123456 + i);
            buyCarUserInformationEntity.setHomeLoansMoney(1234567 + i);
            buyCarUserInformationEntity.setAgeLimit(i);
            buyCarUserInformationEntity.setMonthRefund(123 + i);
            buyCarUserInformationEntity.setUnitType("uuuu" + i);
            buyCarUserInformationEntity.setDutyType("rrrrrrr" + i);
            buyCarUserInformationEntity.setDuty("RRRRRR" + i);
            buyCarUserInformationEntity.setRunTime("1" + i);
            buyCarUserInformationEntity.setWorkTime("2" + i);
            buyCarUserInformationEntity.setHoldShare(0.12f + i);
            buyCarUserInformationEntity.setMonthShare(0.3652f + i);
            buyCarUserInformationEntity.setUserName_1("aierjundddddddddd" + i);
            buyCarUserInformationEntity.setPhoneNumber_1("1785555555" + i);
            buyCarUserInformationEntity.setRekation_1("兄弟121" + i);
            buyCarUserInformationEntity.setUserName_2("aierjunggggggggg" + i);
            buyCarUserInformationEntity.setPhoneNumber_2("178525555555" + i);
            buyCarUserInformationEntity.setRekation_2("兄弟12122" + i);
            buyCarUserInformationEntity.setRemark("");
            buyCarUserInformationEntityList.add(buyCarUserInformationEntity);
        }
    }

    private void test3() {
        spouseInformationEntityList.clear();
        for (int i = 0; i < 1; i++) {
            SpouseInformationEntity spouseInformationEntity = new SpouseInformationEntity();
            spouseInformationEntity.setName("aierjun" + i);
            spouseInformationEntity.setSex("男" + i);
            spouseInformationEntity.setAge("" + i);
            spouseInformationEntity.setCardNumber("420528" + i);
            spouseInformationEntity.setCardAddress("djslajd" + i);
            spouseInformationEntity.setEducational("大专" + i);
            spouseInformationEntity.setCensusRegister("jdsljalf" + i);
            spouseInformationEntity.setPhoneNumber("1785886115" + i);
            spouseInformationEntity.setAddressNow("kkkkkk" + i);
            spouseInformationEntity.setTelPhone("0717-1231346" + i);
            spouseInformationEntity.setUnitType("uuuu" + i);
            spouseInformationEntity.setDutyType("rrrrrrr" + i);
            spouseInformationEntity.setDuty("RRRRRR" + i);
            spouseInformationEntity.setRunTime("1" + i);
            spouseInformationEntity.setMonthShare(0.3652f + i);
            spouseInformationEntity.setRemark("");
            spouseInformationEntityList.add(spouseInformationEntity);
        }
    }
    
    private void test5(){
        carInformationEntityList.clear();
        for (int i = 0 ; i<1 ; i++){
            CarInformationEntity carInformationEntity = new CarInformationEntity();
            carInformationEntity.setCarBigType("pppppp");
            carInformationEntity.setCarType("oooooo");
            carInformationEntity.setSeatNum(5);
            carInformationEntity.setCarNew("iiiiii");
            carInformationEntity.setPublicTag("uuuuuu");
            carInformationEntity.setTagAddress("yyyyy");
            carInformationEntity.setDrivingLicenseName("ttttt");
            carInformationEntity.setLoansBankName("rrrrr");
            carInformationEntity.setLoansTime("eeeee");
            carInformationEntity.setProductType("wwwww");
            carInformationEntity.setProductNumber("qqqqqq");
            carInformationEntity.setCarMoney(0123.5f);
            carInformationEntity.setLoansMoney(12345.0000f);
            carInformationEntity.setCompanyRate(456.23f);
            carInformationEntity.setFirstPayment(4542.45f);
            carInformationEntity.setFirstPaymentRatio(78965.235f);
            carInformationEntity.setLoansRatio(45641631.252f);
            carInformationEntity.setLoansAllMoney(0.2525f);
            carInformationEntity.setLoandStagesRatio(45613f);
            carInformationEntity.setStagesMoney(461.25452f);
            carInformationEntity.setStagesAllMoney(45613f);
            carInformationEntity.setStagesMoneyRatio(123465.123f);
            carInformationEntity.setMonthRepayment(123456.45613f);
            carInformationEntity.setFirstRepayment(123458f);
            carInformationEntity.setRemank("");
            carInformationEntityList.add(carInformationEntity);
        }
    }

    private void test6(){
        checkResultEntityList.clear();
        for (int i = 0;i<1;i++){
            CheckResultEntity checkResultEntity = new CheckResultEntity();
            checkResultEntity.setResultName("征信结果");
            checkResultEntity.setCheckUserName("aierjun");
            checkResultEntity.setRemank("ok very good");
            checkResultEntityList.add(checkResultEntity);
        }
    }

    private void test8(){
        afterLoaningInformationEntityList.clear();
        for (int i = 0;i<1;i++){
            AfterLoaningInformationEntity afterLoaningInformationEntity = new AfterLoaningInformationEntity();
            afterLoaningInformationEntity.setCardID("aaaa");
            afterLoaningInformationEntity.setCardSendAddress("ssss");
            afterLoaningInformationEntity.setCardSendTime("dddd");
            afterLoaningInformationEntity.setCardAcceptTime("ffff");
            afterLoaningInformationEntity.setCompanyGiveTime("gggg");
            afterLoaningInformationEntity.setContractSendBankTime("hhhhh");
            afterLoaningInformationEntity.setCarReachTime("jjjjj");
            afterLoaningInformationEntity.setRegisterCredentialReachTime("kkkkk");
            afterLoaningInformationEntity.setBankGiveTime("llllll");
            afterLoaningInformationEntity.setSendInformationTime("pppp");
            afterLoaningInformationEntity.setPledgeTransaction("ooooo");
            afterLoaningInformationEntityList.add(afterLoaningInformationEntity);
        }
    }
}
