package com.aierjun.car100.main;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.utils.BitmapUtil;
import com.aierjun.car100.utils.GetPathFromUri4kitkat;
import com.aierjun.car100.view.ImageChangeDialog;
import com.aierjun.car100.view.MyDelWeightDialog;
import com.aierjun.car100.view.PickerView;
import com.aierjun.car100.view.PickerViewDialog;
import com.sam.widget.switcher.SwitchButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/11.
 */
/*
* 征信发起
* */

public class CreditCommitActivity extends Activity implements View.OnClickListener {
    private ImageView back;
    private TextView commit;
    private TextView bank;
    private ImageView image_1, image_2, image_3, image_4;
    private ImageView image_1_del, image_2_del, image_3_del, image_4_del;
    private TextView userName;
    private TextView userNumber;
    private TextView add_user;
    private LinearLayout creditcommit_main_l;
    //wife
    private TextView bank_w;
    private ImageView image_1_w, image_2_w, image_3_w, image_4_w;
    private ImageView image_1_del_w, image_2_del_w, image_3_del_w, image_4_del_w;
    private TextView userName_w;
    private TextView userNumber_w;

    private SwitchButton switchButton;

    private MyDelWeightDialog myDelWeightDialog;
    private ImageChangeDialog imageChangeDialog;
    private ImageView photoAndCream;
    private LinearLayout wife_layout;

    private int index = 12;
    private PickerViewDialog pickerViewDialog;
    private List listBank;
    private String pickerView_Text_1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creditcommit);
        finView();
        initView();
    }

    private void finView() {
        creditcommit_main_l = (LinearLayout) findViewById(R.id.creditcommit_main_l);
        back = (ImageView) findViewById(R.id.back);
        commit = (TextView) findViewById(R.id.commit);
        bank = (TextView) findViewById(R.id.car_type_text);
        image_1 = (ImageView) findViewById(R.id.photo_1);
        image_1_del = (ImageView) findViewById(R.id.photo_1_del);
        image_2 = (ImageView) findViewById(R.id.photo_2);
        image_2_del = (ImageView) findViewById(R.id.photo_2_del);
        image_3 = (ImageView) findViewById(R.id.photo_3);
        image_3_del = (ImageView) findViewById(R.id.photo_3_del);
        image_4 = (ImageView) findViewById(R.id.photo_4);
        image_4_del = (ImageView) findViewById(R.id.photo_4_del);
        userName = (TextView) findViewById(R.id.user_name_text);
        userNumber = (TextView) findViewById(R.id.user_number_text);
        switchButton = (SwitchButton) findViewById(R.id.wife_button);
        add_user = (TextView) findViewById(R.id.add_user);
        wife_layout = (LinearLayout) findViewById(R.id.wife_layout);
        //wife
        bank_w = (TextView) findViewById(R.id.car_type_text_w);
        image_1_w = (ImageView) findViewById(R.id.photo_1_w);
        image_1_del_w = (ImageView) findViewById(R.id.photo_1_del_w);
        image_2_w = (ImageView) findViewById(R.id.photo_2_w);
        image_2_del_w = (ImageView) findViewById(R.id.photo_2_del_w);
        image_3_w = (ImageView) findViewById(R.id.photo_3_w);
        image_3_del_w = (ImageView) findViewById(R.id.photo_3_del_w);
        image_4_w = (ImageView) findViewById(R.id.photo_4_w);
        image_4_del_w = (ImageView) findViewById(R.id.photo_4_del_w);
        userName_w = (TextView) findViewById(R.id.user_name_text_w);
        userNumber_w = (TextView) findViewById(R.id.user_number_text_w);
    }

    private void initView() {
        back.setOnClickListener(this);
        commit.setOnClickListener(this);
        bank.setOnClickListener(this);
        image_1.setOnClickListener(this);
        image_1_del.setOnClickListener(this);
        image_2.setOnClickListener(this);
        image_2_del.setOnClickListener(this);
        image_3.setOnClickListener(this);
        image_3_del.setOnClickListener(this);
        image_4.setOnClickListener(this);
        image_4_del.setOnClickListener(this);
        //wife
        bank_w.setOnClickListener(this);
        image_1_w.setOnClickListener(this);
        image_1_del_w.setOnClickListener(this);
        image_2_w.setOnClickListener(this);
        image_2_del_w.setOnClickListener(this);
        image_3_w.setOnClickListener(this);
        image_3_del_w.setOnClickListener(this);
        image_4_w.setOnClickListener(this);
        image_4_del_w.setOnClickListener(this);

        add_user.setOnClickListener(this);

        switchButton.setOnCheckChangeListener(new SwitchButton.OnCheckChangeListener() {
            @Override
            public void OnCheck(SwitchButton switchButton, boolean b) {
                if (b)
                    wife_layout.setVisibility(View.VISIBLE);
                else
                    wife_layout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                if (myDelWeightDialog == null) {
                    myDelWeightDialog = new MyDelWeightDialog(this) {
                        @Override
                        protected void checkLeft(TextView textView) {
                            //取消
                            Log.d("Jun ", "取消");
                            myDelWeightDialog.dismiss();
                            finish();
                        }

                        @Override
                        protected void checkRight(TextView textView) {
                            //保存
                            Log.d("Jun ", "保存");
                            myDelWeightDialog.dismiss();
                            finish();
                        }
                    };
                    myDelWeightDialog.setDelMes("当前未保存，确定继续退出？");
                    myDelWeightDialog.setLeftMes("取消");
                    myDelWeightDialog.setRightMes("保存");
                    myDelWeightDialog.show();
                }
                break;
            case R.id.commit:
                //提交
                break;
            case R.id.car_type_text:
                chooseBank();
                break;
            case R.id.photo_1:
                photoCreamChoose(v);
                break;
            case R.id.photo_1_del:
                break;
            case R.id.photo_2:
                photoCreamChoose(v);
                break;
            case R.id.photo_2_del:
                break;
            case R.id.photo_3:
                photoCreamChoose(v);
                break;
            case R.id.photo_3_del:
                break;
            case R.id.photo_4:
                photoCreamChoose(v);
                break;
            case R.id.photo_4_del:
                break;
            //wife
            case R.id.car_type_text_w:
                chooseBank();
                break;
            case R.id.photo_1_w:
                photoCreamChoose(v);
                break;
            case R.id.photo_1_del_w:
                break;
            case R.id.photo_2_w:
                photoCreamChoose(v);
                break;
            case R.id.photo_2_del_w:
                break;
            case R.id.photo_3_w:
                photoCreamChoose(v);
                break;
            case R.id.photo_3_del_w:
                break;
            case R.id.photo_4_w:
                photoCreamChoose(v);
                break;
            case R.id.photo_4_del_w:
                break;

            case R.id.add_user:
                addUserView();
                break;
            case R.id.del_user:
                delUserView(v);
                break;
            default:

                break;
        }
    }

    private void photoCreamChoose(View v) {
        photoAndCream = (ImageView) v;
        if (imageChangeDialog == null)
            imageChangeDialog = new ImageChangeDialog(CreditCommitActivity.this);
        imageChangeDialog.showGravity(Gravity.BOTTOM).show();
        imageChangeDialog.setOnClickLister(new ImageChangeDialog.OnClickLister() {
            @Override
            public void CLickCream(View view) {
                Log.d("Jun ", "CLickCream");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);
            }

            @Override
            public void ClickPhoto(View view) {
                Log.d("Jun ", "ClickPhoto");
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);//调用android的图库
                startActivityForResult(intent, 2);
            }

            @Override
            public void ClickCancal(View view) {
                Log.d("Jun ", "ClickCancal");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                try {
                    if (data == null || data.getExtras() == null) return;
                    Bundle bundle = data.getExtras();
                    Bitmap bitmap = (Bitmap) bundle.get("data");// 获取相机返回的数据，并转换为Bitmap图片格式
                    photoAndCream.setImageBitmap(BitmapUtil.rotateBitmapSize(bitmap));
                } catch (Exception e) {
                    System.out.println("请拍照");
                }
                break;
            case 2:
                try {
                    if (data == null || data.getData() == null) return;
                    Uri uri = data.getData();
                    Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
                    cursor.moveToFirst();
                    String imgNo = cursor.getString(0); // 图片编号
                    String imgPath = cursor.getString(1); // 图片文件路径
                    String imgSize = cursor.getString(2); // 图片大小
                    String imgName = cursor.getString(3); // 图片文件名
                    String path = GetPathFromUri4kitkat.getPath(this, uri);
                    if (path.equals("") || path == null)
                        path = imgPath;
                    Bitmap b = BitmapFactory.decodeFile(path);
                    photoAndCream.setImageBitmap(BitmapUtil.rotateBitmapSize(b));
                } catch (Exception e) {
                    System.out.println("请选择图片");
                }
                break;
        }
    }

    private View addUserView() {
        View view = View.inflate(this, R.layout.activity_credit_include, null);
        creditcommit_main_l.addView(view, index);
        TextView bank = (TextView) view.findViewById(R.id.car_type_text);
        ImageView photo_1 = (ImageView) view.findViewById(R.id.photo_1);
        ImageView photo_2 = (ImageView) view.findViewById(R.id.photo_2);
        ImageView photo_3 = (ImageView) view.findViewById(R.id.photo_3);
        ImageView photo_4 = (ImageView) view.findViewById(R.id.photo_4);
        TextView user_name_text = (TextView) view.findViewById(R.id.user_name_text);
        TextView user_number_text = (TextView) view.findViewById(R.id.user_number_text);
        TextView del_user = (TextView) view.findViewById(R.id.del_user);
        photo_1.setOnClickListener(this);
        photo_2.setOnClickListener(this);
        photo_3.setOnClickListener(this);
        photo_4.setOnClickListener(this);
        del_user.setOnClickListener(this);
        bank.setOnClickListener(this);
        index++;
        return view;
    }

    private void delUserView(View view) {
        View viewChild = (View) view.getParent();
        creditcommit_main_l.removeView(viewChild);
        index--;
    }

    private void chooseBank() {
        if (listBank == null)
            listBank = new ArrayList();
        listBank.clear();
        listBank.add("中国农业银行股份有限公司萧山分行");
        listBank.add("中国工商银行股份有限公司南京城南支行");
        listBank.add("（2017）中国工商银行股份有限公司杭州延中支行");
        if (pickerViewDialog == null)
            pickerViewDialog = new PickerViewDialog(this, 1);
        pickerViewDialog.setData(listBank, new PickerView.onSelectListener() {
            @Override
            public void onSelect(String text) {
                pickerView_Text_1 = text;
                Log.d("Jun", text);
            }
        });
        pickerViewDialog.setOnClickLister(new PickerViewDialog.OnClickLister() {
            @Override
            public void CLickLeft(View view) {
                pickerViewDialog.dismiss();
            }

            @Override
            public void ClickRight(View view) {
                if (pickerView_Text_1 == null || pickerView_Text_1.equals(""))
                    pickerView_Text_1 = "";
                bank.setText(pickerView_Text_1);
                pickerViewDialog.dismiss();
            }
        });
        pickerViewDialog.showGravity(Gravity.BOTTOM).show();
    }


    @Override
    protected void onDestroy() {
        pickerViewDialog = null;
        myDelWeightDialog = null;
        super.onDestroy();
    }
}

