package com.aierjun.car100.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.init.MyApplication;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/19.
 */

public class BaseActivity extends Activity {
    protected ImageView imageView;
    protected TextView textView;
    protected CallBack callBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void selBackImg() {
        imageView = (ImageView) findViewById(R.id.back);
        if (imageView != null)
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBack != null)
                        callBack.callBackSelLeftImg((ImageView) v);
                    else
                        finish();
                }
            });
    }

    protected void selCommitText() {
        textView = (TextView) findViewById(R.id.commit);
        if (textView != null)
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBack != null)
                        callBack.callBackSelTRightext((TextView) v);
                    else
                        finish();
                }
            });
    }

    protected interface CallBack {
        void callBackSelLeftImg(ImageView imageView);

        void callBackSelTRightext(TextView textView);

        void callBackSelBackPressed();
    }

    protected void setCallBackSel(CallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onBackPressed() {
        if (callBack != null)
            callBack.callBackSelBackPressed();
        else
            super.onBackPressed();
    }
}
