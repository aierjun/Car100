package com.aierjun.car100.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.adapter.DoingBusinessAdapter;
import com.aierjun.car100.entity.DoingBusinessEntity;
import com.aierjun.car100.view.PickerView;
import com.aierjun.car100.view.PickerViewDialog;
import com.aierjun.car100.widget.RefreshListView;

import java.util.ArrayList;
import java.util.List;

import static android.view.Gravity.BOTTOM;
import static android.view.Gravity.CENTER;

/**
 * Created by Ani_aierJun on 2017/9/19.
 */
/*
* 客户信息查询
* */
public class UserInformationQueryActivity extends BaseActivity implements View.OnClickListener{
    private String TAG = "UserInformationQueryA";
    private EditText userinformation_check_ed;
    private TextView userinformation_chooseBank,userinformation_chooseOrderState,userinformation_chooseLoansPerson;
    private RefreshListView userinformation_listview;
    private List<DoingBusinessEntity> list = new ArrayList<>();
    private List<String> listStr = new ArrayList<>();
    private PickerViewDialog pickerViewDialog;
    private String picperText = "";
    private int pickerNum = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinformation);
        selBackImg();
        findView();
        initView();

    }

    private void findView() {
        userinformation_check_ed = (EditText) findViewById(R.id.userinformation_check_ed);
        userinformation_chooseBank = (TextView) findViewById(R.id.userinformation_chooseBank);
        userinformation_chooseOrderState = (TextView) findViewById(R.id.userinformation_chooseOrderState);
        userinformation_chooseLoansPerson = (TextView) findViewById(R.id.userinformation_chooseLoansPerson);
        userinformation_listview = (RefreshListView) findViewById(R.id.userinformation_listview);
    }

    private void initView() {
        userinformation_chooseBank.setOnClickListener(this);
        userinformation_chooseOrderState.setOnClickListener(this);
        userinformation_chooseLoansPerson.setOnClickListener(this);
        //test
        setListAdapter(testAddData());
    }

    public void setListAdapter(List<DoingBusinessEntity> list){
        DoingBusinessAdapter doingBusinessAdapter = new DoingBusinessAdapter(this,list , new DoingBusinessAdapter.CallBackOnSelLisinter() {
            @Override
            public void callBackViewItem(View view, int item) {
                Log.d(TAG,item + "  " + view.getTag());
            }
        });
        userinformation_listview.setAdapter(doingBusinessAdapter);
        userinformation_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("","");
                intent.setClass(UserInformationQueryActivity.this,OrderDetailsActivity.class);
                startActivity(intent);
            }
        });
        userinformation_listview.setCallBack(new RefreshListView.CallBack() {
            @Override
            public void callBackTop() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        userinformation_listview.stopTOPLoading(true);
                    }
                }, 2000);
            }

            @Override
            public void callBackBotton() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        userinformation_listview.stopBottonLoading(true);
                    }
                }, 1000);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.userinformation_chooseBank:
                showPicker(1);
                break;
            case R.id.userinformation_chooseOrderState:
                showPicker(2);
                break;
            case R.id.userinformation_chooseLoansPerson:
                showPicker(3);
                break;
        }
    }

    private void showPicker(final int position){
        listStr.clear();
        if (position == 1){
            listStr.add("全部");
            listStr.add("中国农业银行股份有限公司萧山分行");
            listStr.add("中国工商银行股份有限公司南京城南支行");
            listStr.add("(2017)中国工商银行股份有限公司杭州延中支行");
        }else if (position  == 2){
            listStr.add("全部");
            listStr.add("征信提交");
            listStr.add("征信已查询");
            listStr.add("初审单提交");
            listStr.add("终审通过");
            listStr.add("退回");
        }else if (position == 3){
            listStr.add("全部");
            listStr.add("测试");
            listStr.add("业务员1");
            listStr.add("业务员2");
        }
        pickerViewDialog = new PickerViewDialog(this,1);
        pickerViewDialog.setData(listStr, new PickerView.onSelectListener() {
            @Override
            public void onSelect(String text) {
                pickerNum = position;
                picperText = text;
                Log.d(TAG,text + position);
            }
        });
        pickerViewDialog.setOnClickLister(new PickerViewDialog.OnClickLister() {
            @Override
            public void CLickLeft(View view) {
                pickerViewDialog.dismiss();
                pickerViewDialog = null;
            }

            @Override
            public void ClickRight(View view) {
                pickerViewDialog.dismiss();
                pickerViewDialog = null;
                if (picperText == null || picperText.equals("")) return;
                if (pickerNum == 1)
                    userinformation_chooseBank.setText(picperText);
                if (pickerNum == 2)
                    userinformation_chooseOrderState.setText(picperText);
                if (pickerNum == 3)
                    userinformation_chooseLoansPerson.setText(picperText);
            }
        });
        pickerViewDialog.showGravity(BOTTOM).show();
    }


    public List<DoingBusinessEntity> testAddData(){
        for (int i = 1 ; i<5;i++){
            DoingBusinessEntity doingBusinessEntity = new DoingBusinessEntity();
            doingBusinessEntity.setState(i);
            doingBusinessEntity.setTime(System.currentTimeMillis());
            doingBusinessEntity.setUserName("aierjun"+i);
            doingBusinessEntity.setUserNumber("" + i);
            list.add(doingBusinessEntity);
        }
        return list;
    }
}
