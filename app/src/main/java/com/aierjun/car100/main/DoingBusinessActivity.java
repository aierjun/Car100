package com.aierjun.car100.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aierjun.car100.R;
import com.aierjun.car100.adapter.DoingBusinessAdapter;
import com.aierjun.car100.entity.DoingBusinessEntity;
import com.aierjun.car100.widget.MyListView;
import com.aierjun.car100.widget.RefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Ani_aierJun on 2017/9/19.
 */
/*
* 进行中业务
* */

public class DoingBusinessActivity extends BaseActivity implements View.OnClickListener{
    private String TAG = "DoingBusinessActivity";
    private EditText doingbusiness_check_ed;
    private TextView doingbusiness_btn_1, doingbusiness_btn_2, doingbusiness_btn_3, doingbusiness_btn_4, doingbusiness_btn_5 , doingbusiness_btn_now;
    private int tagBtn = 1;
    private RefreshListView doingbusiness_listview;
    private List<DoingBusinessEntity> list = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doingbusiness);
        selBackImg();
        findView();
        initView();

    }

    private void findView() {
        doingbusiness_check_ed = (EditText) findViewById(R.id.doingbusiness_check_ed);
        doingbusiness_btn_1 = (TextView) findViewById(R.id.doingbusiness_btn_1);
        doingbusiness_btn_2 = (TextView) findViewById(R.id.doingbusiness_btn_2);
        doingbusiness_btn_3 = (TextView) findViewById(R.id.doingbusiness_btn_3);
        doingbusiness_btn_4 = (TextView) findViewById(R.id.doingbusiness_btn_4);
        doingbusiness_btn_5 = (TextView) findViewById(R.id.doingbusiness_btn_5);
        doingbusiness_listview = (RefreshListView) findViewById(R.id.doingbusiness_listview);
    }

    private void initView() {
        doingbusiness_btn_1.setOnClickListener(this);
        doingbusiness_btn_2.setOnClickListener(this);
        doingbusiness_btn_3.setOnClickListener(this);
        doingbusiness_btn_4.setOnClickListener(this);
        doingbusiness_btn_5.setOnClickListener(this);
        doingbusiness_btn_now = doingbusiness_btn_1;
        doingbusiness_btn_1.setSelected(true);

        testAddData();

        DoingBusinessAdapter doingBusinessAdapter = new DoingBusinessAdapter(this,list , new DoingBusinessAdapter.CallBackOnSelLisinter() {
            @Override
            public void callBackViewItem(View view, int item) {
                Log.d(TAG,item + "  " + view.getTag());
            }
        });
        doingbusiness_listview.setAdapter(doingBusinessAdapter);
        doingbusiness_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("","");
                intent.setClass(DoingBusinessActivity.this,OrderDetailsActivity.class);
                startActivity(intent);
            }
        });
        doingbusiness_listview.setCallBack(new RefreshListView.CallBack() {
            @Override
            public void callBackTop() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doingbusiness_listview.stopTOPLoading(true);
                    }
                }, 2000);
            }

            @Override
            public void callBackBotton() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doingbusiness_listview.stopBottonLoading(true);
                    }
                }, 1000);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.  doingbusiness_btn_1:
                v.setTag(1);
                selBtnShow(v);
                break;
            case R.id.  doingbusiness_btn_2:
                v.setTag(2);
                selBtnShow(v);
                break;
            case R.id.  doingbusiness_btn_3:
                v.setTag(3);
                selBtnShow(v);
                break;
            case R.id.  doingbusiness_btn_4:
                v.setTag(4);
                selBtnShow(v);
                break;
            case R.id.  doingbusiness_btn_5:
                v.setTag(5);
                selBtnShow(v);
                break;
        }
    }

    public void selBtnShow(View v){
        doingbusiness_btn_now.setSelected(false);
        v.setSelected(true);
        doingbusiness_btn_now = (TextView) v;
        tagBtn = (int) v.getTag();
        Log.d(TAG , tagBtn + "");
    }

    public void testAddData(){
        for (int i = 1 ; i<5;i++){
            DoingBusinessEntity doingBusinessEntity = new DoingBusinessEntity();
            doingBusinessEntity.setState(i);
            doingBusinessEntity.setTime(System.currentTimeMillis());
            doingBusinessEntity.setUserName("aierjun"+i);
            doingBusinessEntity.setUserNumber("" + i);
            list.add(doingBusinessEntity);
        }
    }
}
