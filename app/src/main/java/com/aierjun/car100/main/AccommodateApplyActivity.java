package com.aierjun.car100.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.adapter.AccommodateApplyAdapter;
import com.aierjun.car100.entity.AccommodateApplyEntity;
import com.aierjun.car100.widget.RefreshListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/19.
 */
/*通融申请
*
* */
public class AccommodateApplyActivity extends BaseActivity {
    private RefreshListView accommodateapply_listview;
    private List<AccommodateApplyEntity> accommodateApplyEntityList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accommodataapply);
        selBackImg();
        findView();
        initView();
    }

    private void findView() {
        accommodateapply_listview = (RefreshListView) findViewById(R.id.accommodateapply_listview);
    }

    private void initView() {
        test();
        AccommodateApplyAdapter accommodateApplyAdapter = new AccommodateApplyAdapter(this,accommodateApplyEntityList);
        accommodateapply_listview.setAdapter(accommodateApplyAdapter);
    }

    private void test() {
        accommodateApplyEntityList.clear();
        for (int i = 0;i<10;i++){
            AccommodateApplyEntity accommodateApplyEntity = new AccommodateApplyEntity();
            accommodateApplyEntity.setCallbackTime("zzzz");
            accommodateApplyEntity.setState("xxxxx");
            accommodateApplyEntity.setOrderNumber("ccccc");
            accommodateApplyEntity.setUserName("vvvvvv");
            accommodateApplyEntity.setCallbackBeginState("bbbb");
            accommodateApplyEntity.setCheckName("nnnn");
            accommodateApplyEntity.setCallbackReason("mmmm");
            accommodateApplyEntity.setS("oooooo");
            accommodateApplyEntityList.add(accommodateApplyEntity);
        }
    }
}
