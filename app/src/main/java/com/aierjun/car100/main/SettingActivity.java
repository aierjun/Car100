package com.aierjun.car100.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.init.MyApplication;
import com.aierjun.car100.login.ForgetPasswordActivity;
import com.aierjun.car100.login.LoginActivity;
import com.aierjun.car100.utils.ClearCacheUtils;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/14.
 */

public class SettingActivity extends Activity implements View.OnClickListener{
    private RelativeLayout setting_fix_password,setting_version_update,setting_clear_cache;
    private TextView setting_login_exit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        findView();
        initView();
    }

    private void findView() {
        setting_fix_password = (RelativeLayout) findViewById(R.id.setting_fix_password);
        setting_version_update = (RelativeLayout) findViewById(R.id.setting_version_update);
        setting_version_update = (RelativeLayout) findViewById(R.id.setting_clear_cache);
        setting_login_exit = (TextView) findViewById(R.id.setting_login_exit);
    }

    private void initView() {
        setting_fix_password.setOnClickListener(this);
        setting_version_update.setOnClickListener(this);
        setting_version_update.setOnClickListener(this);
        setting_login_exit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.setting_fix_password:
                Intent intent =new Intent();
                intent.setClass(this, ForgetPasswordActivity.class);
                intent.putExtra("Phone","13345645610");
                startActivity(intent);
                break;
            case R.id.setting_version_update:

                break;
            case R.id.setting_clear_cache:
                ClearCacheUtils.clearAll(this);
                break;
            case R.id.setting_login_exit:
                setting_login_exit.setSelected(true);
                //...
                ClearCacheUtils.clearAll(this);
                List<Activity> activityList = MyApplication.getmActivityList();
                activityList.get(activityList.size()-2).finish();
                finish();
                break;
        }
    }
}
