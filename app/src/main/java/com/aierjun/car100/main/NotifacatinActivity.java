package com.aierjun.car100.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.aierjun.car100.R;
import com.aierjun.car100.adapter.NotifacatinAdapter;
import com.aierjun.car100.entity.NotifacationEntity;
import com.aierjun.car100.init.MyApplication;
import com.aierjun.car100.widget.MyListView;
import com.aierjun.car100.widget.RefreshListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/14.
 */

public class NotifacatinActivity extends Activity {
    private RefreshListView notifacation_list;
    private String TAG = "NotifacatinActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifacation);
        findView();
        initView();
    }

    private void findView() {
        notifacation_list = (RefreshListView) findViewById(R.id.notifacation_list);
        notifacation_list.setCallBack(new RefreshListView.CallBack() {
            @Override
            public void callBackTop() {
                Log.d("NotifacatinActivity","callBackTop");
            }

            @Override
            public void callBackBotton() {
                Log.d("NotifacatinActivity","callBackBotton");
            }
        });
    }

    private void initView() {
        List<NotifacationEntity> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            NotifacationEntity entity = new NotifacationEntity();
            entity.setState("征信查询" + i);
            entity.setTime("2017/03/22 03:22:33" + i);
            entity.setNumber("测01-123456-001" + i);
            entity.setName("aierjun" + i);
            list.add(entity);
        }
        NotifacatinAdapter notifacatinAdapter = new NotifacatinAdapter(this, list, new NotifacatinAdapter.NotificationStateOnClickLister() {
            @Override
            public void callBake(View view, int position) {
                Log.d(TAG, position + "");
            }
        });
        notifacation_list.setAdapter(notifacatinAdapter);
        notifacation_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, position + "");
            }
        });
    }
}
