package com.aierjun.car100.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.sam.widget.headerBar.LeftTextTitleRightImageBar;

/**
 * Created by Ani_aierJun on 2017/9/14.
 */

public class MainActivity extends Activity implements View.OnClickListener{
    private String TAG = "MainActivity";
    private LeftTextTitleRightImageBar main_leftTextTitleRightImageBar;
    private TextView main_position, main_name;
    private LinearLayout main_credit, mian_doing, main_rules_commit, main_fix_commit, main_send_information, main_user_query;
    private RelativeLayout main_setting;

    public  @IdRes int titleBarRightID ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_car);
        findView();
        initView();
    }

    private void findView() {
        main_leftTextTitleRightImageBar = (LeftTextTitleRightImageBar) findViewById(R.id.main_leftTextTitleRightImageBar);
        main_position = (TextView) findViewById(R.id.main_position);
        main_name = (TextView) findViewById(R.id.main_name);
        main_credit = (LinearLayout) findViewById(R.id.main_credit);
        mian_doing = (LinearLayout) findViewById(R.id.mian_doing);
        main_rules_commit = (LinearLayout) findViewById(R.id.main_rules_commit);
        main_fix_commit = (LinearLayout) findViewById(R.id.main_fix_commit);
        main_send_information = (LinearLayout) findViewById(R.id.main_send_information);
        main_user_query = (LinearLayout) findViewById(R.id.main_user_query);
        main_setting = (RelativeLayout) findViewById(R.id.main_setting);
    }

    private void initView() {
        main_credit.setOnClickListener(this);
        mian_doing.setOnClickListener(this);
        main_rules_commit.setOnClickListener(this);
        main_fix_commit.setOnClickListener(this);
        main_send_information.setOnClickListener(this);
        main_user_query.setOnClickListener(this);
        main_setting.setOnClickListener(this);
        main_leftTextTitleRightImageBar.getRightView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,NotifacatinActivity.class));
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.main_credit:
                startActivity(new Intent(this,CreditCommitActivity.class));
                break;
            case R.id.mian_doing:
                startActivity(new Intent(this,DoingBusinessActivity.class));
                break;
            case R.id.main_rules_commit:
                startActivity(new Intent(this,AccommodateApplyActivity.class));
                break;
            case R.id.main_fix_commit:
                startActivity(new Intent(this,FixApplyActivity.class));
                break;
            case R.id.main_send_information:
                startActivity(new Intent(this,SendInformationActivity.class));
                break;
            case R.id.main_user_query:
                startActivity(new Intent(this,UserInformationQueryActivity.class));
                break;
            case R.id.main_setting:
                startActivity(new Intent(MainActivity.this,SettingActivity.class));
                break;
        }
    }
}
