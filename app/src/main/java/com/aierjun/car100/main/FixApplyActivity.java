package com.aierjun.car100.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.aierjun.car100.R;
import com.aierjun.car100.adapter.FixApplyAdapter;
import com.aierjun.car100.entity.FixApplyEntity;
import com.aierjun.car100.widget.RefreshListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/19.
 */
/*
* 修改申请
* */

public class FixApplyActivity extends BaseActivity {
    private String TAG = "FixApplyActivity";
    private RefreshListView fixapply_listview;
    private EditText fixapply_check_ed;
    private List<FixApplyEntity> fixApplyEntityList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixapply);
        selBackImg();
        findView();
        initView();

    }

    private void findView() {
        fixapply_listview = (RefreshListView) findViewById(R.id.fixapply_listview);
        fixapply_check_ed = (EditText) findViewById(R.id.fixapply_check_ed);
    }

    private void initView() {
        test();
        FixApplyAdapter fixApplyAdapter = new FixApplyAdapter(this, fixApplyEntityList, new FixApplyAdapter.CallBackOnSelLisinter() {
            @Override
            public void callBackViewItem(View view, int item) {
                Log.d(TAG,item + "");
            }
        });
        fixapply_listview.setAdapter(fixApplyAdapter);
    }

    private void test() {
        fixApplyEntityList.clear();
        for (int i=0;i<10 ;i++){
            FixApplyEntity fixApplyEntity = new FixApplyEntity();
            fixApplyEntity.setTime("123456");
            fixApplyEntity.setState("rrrrr");
            fixApplyEntity.setUserNumber("007" + i);
            fixApplyEntity.setUserName("aierjun");
            fixApplyEntityList.add(fixApplyEntity);
        }
    }
}
