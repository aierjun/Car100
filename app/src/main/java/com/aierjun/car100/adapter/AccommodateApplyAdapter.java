package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.AccommodateApplyEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class AccommodateApplyAdapter extends BaseAdapter {
    private Context context;
    private List<AccommodateApplyEntity> list;

    public AccommodateApplyAdapter(Context context, List<AccommodateApplyEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.accomodateapply_layout_item,null);
            viewHolder.callbackTime = (TextView) convertView.findViewById(R.id.accomodateapply_item_callbackTime);
            viewHolder.state = (TextView) convertView.findViewById(R.id.accomodateapply_item_state);
            viewHolder.orderNumber = (TextView) convertView.findViewById(R.id.accomodateapply_item_orderNumber);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.accomodateapply_item_userName);
            viewHolder.callbackBeginState = (TextView) convertView.findViewById(R.id.accomodateapply_item_callbackBeginState);
            viewHolder.checkName = (TextView) convertView.findViewById(R.id.accomodateapply_item_checkName);
            viewHolder.callbackReason = (TextView) convertView.findViewById(R.id.accomodateapply_item_callbackReason);
            viewHolder.s = (TextView) convertView.findViewById(R.id.accomodateapply_item_s);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        AccommodateApplyEntity entity = (AccommodateApplyEntity) getItem(position);
        viewHolder.callbackTime.setText(entity.getCallbackTime());
        viewHolder.state.setText(entity.getState());
        viewHolder.orderNumber.setText(entity.getOrderNumber());
        viewHolder.userName.setText(entity.getUserName());
        viewHolder.callbackBeginState.setText(entity.getCallbackBeginState());
        viewHolder.checkName.setText(entity.getCheckName());
        viewHolder.callbackReason.setText(entity.getCallbackReason());
        viewHolder.callbackReason.setText(entity.getS());
        return convertView;
    }
    
    class ViewHolder{
        private TextView callbackTime;// 退回时间
        private TextView state;//状态
        private TextView orderNumber;//订单号
        private TextView userName;//客户名
        private TextView callbackBeginState;//退回前状态
        private TextView checkName;//审核人
        private TextView callbackReason;//退回原因
        private TextView s;
    }
}
