package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.BasicInformationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class BasicInformationAdapter extends BaseAdapter {
    private Context context;
    private List<BasicInformationEntity> list;

    public BasicInformationAdapter(Context context, List<BasicInformationEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.basicinformation_layout_item,null);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.basicinformation_item_username);
            viewHolder.userNumber = (TextView) convertView.findViewById(R.id.basicinformation_item_usernumber);
            viewHolder.signOpertator = (TextView) convertView.findViewById(R.id.basicinformation_item_signoperator);
            viewHolder.opertaor = (TextView) convertView.findViewById(R.id.basicinformation_item_opertaor);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        BasicInformationEntity entity = (BasicInformationEntity) getItem(position);
        viewHolder.userName.setText(entity.getUserName());
        viewHolder.userNumber.setText(entity.getUserNumber());
        viewHolder.signOpertator.setText(entity.getSignOperator());
        viewHolder.opertaor.setText(entity.getOperator());
        return convertView;
    }

    class ViewHolder {
        private TextView userName;
        private TextView userNumber;
        private TextView signOpertator;
        private TextView opertaor;
    }
}
