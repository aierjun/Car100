package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.aierjun.car100.entity.GuaranteeInformationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class GuaranteeInformationAdapter extends BaseAdapter {
    private Context context;
    private List<GuaranteeInformationEntity> list;

    public GuaranteeInformationAdapter(Context context, List<GuaranteeInformationEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return convertView;
    }
}
