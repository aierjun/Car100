package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.aierjun.car100.entity.OverdueInformationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */
/*
* 逾期情况
* */
public class OverdueInformationAdapter extends BaseAdapter{
    private Context context;
    private List<OverdueInformationEntity> list;

    public OverdueInformationAdapter(Context context, List<OverdueInformationEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return convertView;
    }
}
