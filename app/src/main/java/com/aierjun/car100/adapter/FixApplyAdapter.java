package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.FixApplyEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class FixApplyAdapter extends BaseAdapter {
    private Context context;
    private List<FixApplyEntity> list;
    private CallBackOnSelLisinter callBackOnSelLisinter;

    public FixApplyAdapter(Context context, List<FixApplyEntity> list,CallBackOnSelLisinter callBackOnSelLisinter) {
        this.context = context;
        this.list = list;
        this.callBackOnSelLisinter = callBackOnSelLisinter;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.fixapply_layout_item,null);
            viewHolder.time = (TextView) convertView.findViewById(R.id.fixapply_item_time);
            viewHolder.state = (TextView) convertView.findViewById(R.id.fixapply_item_state);
            viewHolder.userNumber = (TextView) convertView.findViewById(R.id.fixapply_item_userNumber);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.fixapply_item_userName);
            viewHolder.btn = (TextView) convertView.findViewById(R.id.fixapply_item_btn);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        FixApplyEntity entity = (FixApplyEntity) getItem(position);
        viewHolder.time.setText(entity.getTime());
        viewHolder.state.setText(entity.getState());
        viewHolder.userNumber.setText(entity.getUserNumber());
        viewHolder.userName.setText(entity.getUserName());
        viewHolder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBackOnSelLisinter.callBackViewItem(v,position);
            }
        });
        return convertView;
    }
    
    class ViewHolder{
        private TextView time;//时间
        private TextView state;//状态
        private TextView userNumber;//客户编号
        private TextView userName;//客户名
        private TextView btn;//申请修改订单
    }

    public interface CallBackOnSelLisinter{
        void callBackViewItem(View view , int item);
    }
}
