package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.DoingBusinessEntity;
import com.aierjun.car100.utils.DateTimeUtils;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class DoingBusinessAdapter extends BaseAdapter {
    private Context context;
    private List<DoingBusinessEntity> list;
    private CallBackOnSelLisinter callBackOnSelLisinter;

    public DoingBusinessAdapter(Context context, List<DoingBusinessEntity> list ,CallBackOnSelLisinter callBackOnSelLisinter) {
        this.context = context;
        this.list = list;
        this.callBackOnSelLisinter = callBackOnSelLisinter;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.doingbusiness_layout_item,null);
            viewHolder.state_img_1 = (ImageView) convertView.findViewById(R.id.doingbusiness_item_img_1);
            viewHolder.state_img_2 = (ImageView) convertView.findViewById(R.id.doingbusiness_item_img_2);
            viewHolder.state_img_3 = (ImageView) convertView.findViewById(R.id.doingbusiness_item_img_3);
            viewHolder.state_img_4 = (ImageView) convertView.findViewById(R.id.doingbusiness_item_img_4);
            viewHolder.state_btn_1 = (TextView) convertView.findViewById(R.id.doingbusiness_item_btn_1);
            viewHolder.state_btn_2 = (TextView) convertView.findViewById(R.id.doingbusiness_item_btn_2);
            viewHolder.state_btn_3 = (TextView) convertView.findViewById(R.id.doingbusiness_item_btn_3);
            viewHolder.state_btn_4 = (TextView) convertView.findViewById(R.id.doingbusiness_item_btn_4);
            viewHolder.state_btn_5 = (TextView) convertView.findViewById(R.id.doingbusiness_item_btn_5);
            viewHolder.time = (TextView) convertView.findViewById(R.id.doingbusiness_item_time);
            viewHolder.userNumber = (TextView) convertView.findViewById(R.id.doingbusiness_item_usernumber);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.doingbusiness_item_username);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        DoingBusinessEntity entity = (DoingBusinessEntity) getItem(position);
        if (entity.getState() == 3){
            viewHolder.state_btn_3.setVisibility(View.INVISIBLE);
            viewHolder.state_btn_4.setVisibility(View.INVISIBLE);
            viewHolder.state_btn_5.setText("查看物流");
        }else{
            viewHolder.state_btn_3.setVisibility(View.VISIBLE);
            viewHolder.state_btn_4.setVisibility(View.VISIBLE);
            viewHolder.state_btn_5.setText("初审单录入");
        }
        stateShow(viewHolder,entity.getState());
        viewHolder.time.setText(DateTimeUtils.getFormatDateTime(entity.getTime()));
        viewHolder.userNumber.setText(entity.getUserNumber());
        viewHolder.userName.setText(entity.getUserName());
        selBtnCallBack(viewHolder,position);
        return convertView;
    }

    class ViewHolder {
        private ImageView state_img_1;
        private ImageView state_img_2;
        private ImageView state_img_3;
        private ImageView state_img_4;
        private TextView state_btn_1;
        private TextView state_btn_2;
        private TextView state_btn_3;
        private TextView state_btn_4;
        private TextView state_btn_5;
        private TextView time; //提交时间
        private TextView userNumber; //客户编号
        private TextView userName;//客户名
    }

    private void stateShow(ViewHolder viewHolder,int state){
        switch (state){
            case 1:
                viewHolder.state_img_1.setVisibility(View.VISIBLE);
                viewHolder.state_img_2.setVisibility(View.GONE);
                viewHolder.state_img_3.setVisibility(View.GONE);
                viewHolder.state_img_4.setVisibility(View.GONE);
                viewHolder.state_btn_1.setVisibility(View.INVISIBLE);
                viewHolder.state_btn_2.setVisibility(View.INVISIBLE);
                break;
            case 2:
                viewHolder.state_img_1.setVisibility(View.GONE);
                viewHolder.state_img_2.setVisibility(View.VISIBLE);
                viewHolder.state_img_3.setVisibility(View.GONE);
                viewHolder.state_img_4.setVisibility(View.GONE);
                viewHolder.state_btn_1.setVisibility(View.VISIBLE);
                viewHolder.state_btn_2.setVisibility(View.INVISIBLE);
                break;
            case 3:
                viewHolder.state_img_1.setVisibility(View.GONE);
                viewHolder.state_img_2.setVisibility(View.GONE);
                viewHolder.state_img_3.setVisibility(View.VISIBLE);
                viewHolder.state_img_4.setVisibility(View.GONE);
                viewHolder.state_btn_1.setVisibility(View.VISIBLE);
                viewHolder.state_btn_2.setVisibility(View.INVISIBLE);
                break;
            case 4:
                viewHolder.state_img_1.setVisibility(View.GONE);
                viewHolder.state_img_2.setVisibility(View.GONE);
                viewHolder.state_img_3.setVisibility(View.GONE);
                viewHolder.state_img_4.setVisibility(View.GONE);
                viewHolder.state_btn_1.setVisibility(View.VISIBLE);
                viewHolder.state_btn_2.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public void selBtnCallBack(ViewHolder viewHolder,final int item){
        viewHolder.state_btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBackOnSelLisinter.callBackViewItem(v,item);
            }
        });
        viewHolder.state_btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBackOnSelLisinter.callBackViewItem(v,item);
            }
        });
        viewHolder.state_btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBackOnSelLisinter.callBackViewItem(v,item);
            }
        });
        viewHolder.state_btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBackOnSelLisinter.callBackViewItem(v,item);
            }
        });
        viewHolder.state_btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBackOnSelLisinter.callBackViewItem(v,item);
            }
        });
    }

    public interface CallBackOnSelLisinter{
        void callBackViewItem(View view , int item);
    }
}
