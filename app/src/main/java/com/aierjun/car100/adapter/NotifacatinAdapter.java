package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.NotifacationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/14.
 */

public class NotifacatinAdapter extends BaseAdapter {
    private Context context;
    private List<NotifacationEntity> list;
    private NotificationStateOnClickLister notificationStateOnClickLister;

    public NotifacatinAdapter(Context context, List<NotifacationEntity> list , NotificationStateOnClickLister notificationStateOnClickLister) {
        this.context = context;
        this.list = list;
        this.notificationStateOnClickLister = notificationStateOnClickLister;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.notifacation_layout_item, null);
            viewHolder.state = (TextView) convertView.findViewById(R.id.state);
            viewHolder.time = (TextView) convertView.findViewById(R.id.time);
            viewHolder.number = (TextView) convertView.findViewById(R.id.number);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) convertView.getTag();
        NotifacationEntity notifacationEntity = (NotifacationEntity) getItem(position);
        viewHolder.state.setText(notifacationEntity.getState());
        viewHolder.time.setText(notifacationEntity.getTime());
        viewHolder.number.setText(notifacationEntity.getNumber());
        viewHolder.name.setText(notifacationEntity.getName());
        viewHolder.state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notificationStateOnClickLister != null)
                    notificationStateOnClickLister.callBake(v,position);
            }
        });
        return convertView;
    }

    class ViewHolder {
        private TextView state;
        private TextView time;
        private TextView number;
        private TextView name;
    }

    public interface NotificationStateOnClickLister {
        void callBake(View view, int position);
    }
}
