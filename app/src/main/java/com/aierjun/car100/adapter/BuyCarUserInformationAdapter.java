package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.BuyCarUserInformationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class BuyCarUserInformationAdapter extends BaseAdapter {
    private Context context;
    private List<BuyCarUserInformationEntity> list;

    public BuyCarUserInformationAdapter(Context context, List<BuyCarUserInformationEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.buycaruserinformation_layout_item, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_buycarusername);
            viewHolder.sex = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_sex);
            viewHolder.age = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_age);
            viewHolder.cardNumber = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_cardNumber);
            viewHolder.cardAddress = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_cardAddress);
            viewHolder.educational = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_educational);
            viewHolder.censusRegister = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_censusRegister);
            viewHolder.phoneNumber = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_phoneNumber);
            viewHolder.marry = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_marry);
            viewHolder.addressNow = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_addressNow);
            viewHolder.telPhone = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_telPhone);
            viewHolder.homeAddress = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_homeAddress);
            viewHolder.homeRightAddress = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_homeRightAddress);
            viewHolder.homeRightUserName = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_homeRightUserName);
            viewHolder.bugCarRelation = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_bugCarRelation);
            viewHolder.homeType = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_homeType);
            viewHolder.homeSize = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_homeSize);
            viewHolder.homeMoney = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_homeMoney);
            viewHolder.homeLoansMoney = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_homeLoansMoney);
            viewHolder.ageLimit = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_ageLimit);
            viewHolder.monthRefund = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_monthRefund);
            viewHolder.unitType = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_unitType);
            viewHolder.dutyType = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_dutyType);
            viewHolder.duty = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_duty);
            viewHolder.runTime = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_runTime);
            viewHolder.workTime = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_workTime);
            viewHolder.holdShare = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_holdShare);
            viewHolder.monthShare = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_monthShare);
            viewHolder.userName_1 = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_userName_1);
            viewHolder.phoneNumber_1 = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_phoneNumber_1);
            viewHolder.rekation_1 = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_rekation_1);
            viewHolder.userName_2 = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_userName_2);
            viewHolder.phoneNumber_2 = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_phoneNumber_2);
            viewHolder.rekation_2 = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_rekation_2);
            viewHolder.remark = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_remark);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        BuyCarUserInformationEntity entity = (BuyCarUserInformationEntity) getItem(position);
        viewHolder.name.setText(entity.getName());
        viewHolder.sex.setText(entity.getSex());
        viewHolder.age.setText(entity.getAge());
        viewHolder.cardNumber.setText(entity.getCardNumber());
        viewHolder.cardAddress.setText(entity.getCardAddress());
        viewHolder.educational.setText(entity.getEducational());
        viewHolder.censusRegister.setText(entity.getCensusRegister());
        viewHolder.phoneNumber.setText(entity.getPhoneNumber());
        viewHolder.marry.setText(entity.getMarry());
        viewHolder.addressNow.setText(entity.getAddressNow());
        viewHolder.telPhone.setText(entity.getTelPhone());
        viewHolder.homeAddress.setText(entity.getHomeAddress());
        viewHolder.homeRightAddress.setText(entity.getHomeRightAddress());
        viewHolder.homeRightUserName.setText(entity.getHomeRightUserName());
        viewHolder.bugCarRelation.setText(entity.getBugCarRelation());
        viewHolder.homeType.setText(entity.getHomeType());
        viewHolder.homeSize.setText(entity.getHomeSize() + "");
        viewHolder.homeMoney.setText(entity.getHomeMoney() + "");
        viewHolder.homeLoansMoney.setText(entity.getHomeLoansMoney() + "");
        viewHolder.ageLimit.setText(entity.getAgeLimit() + "");
        viewHolder.monthRefund.setText(entity.getMonthRefund() + "");
        viewHolder.unitType.setText(entity.getUnitType());
        viewHolder.dutyType.setText(entity.getDutyType());
        viewHolder.duty.setText(entity.getDuty());
        viewHolder.runTime.setText(entity.getRunTime());
        viewHolder.workTime.setText(entity.getWorkTime());
        viewHolder.holdShare.setText(entity.getHoldShare() + "");
        viewHolder.monthShare.setText(entity.getMonthShare() + "");
        viewHolder.userName_1.setText(entity.getUserName_1());
        viewHolder.phoneNumber_1.setText(entity.getPhoneNumber_1());
        viewHolder.rekation_1.setText(entity.getRekation_1());
        viewHolder.userName_2.setText(entity.getUserName_2());
        viewHolder.phoneNumber_2.setText(entity.getPhoneNumber_2());
        viewHolder.rekation_2.setText(entity.getRekation_2());
        viewHolder.remark.setText(entity.getRemark());
        return convertView;
    }

    class ViewHolder {
        private TextView name; //姓名
        private TextView sex; //性别
        private TextView age;//年龄
        private TextView cardNumber;//身份证号码
        private TextView cardAddress;//身份证归属地
        private TextView educational;//学历
        private TextView censusRegister;//户籍
        private TextView phoneNumber;//电话号码
        private TextView marry;//婚否
        private TextView addressNow;//现居地
        private TextView telPhone;//固定电话
        private TextView homeAddress;//实际家庭住址
        private TextView homeRightAddress;//房产地址
        private TextView homeRightUserName;//房产所有权人
        private TextView bugCarRelation;//与购车人关系
        private TextView homeType;//房产性质
        private TextView homeSize;//房屋面积
        private TextView homeMoney;//目前市价
        private TextView homeLoansMoney;//房贷金额
        private TextView ageLimit;//年限
        private TextView monthRefund;//月还款
        private TextView unitType;//单位性质
        private TextView dutyType;//职务类型
        private TextView duty;//职务
        private TextView runTime;//经营期限
        private TextView workTime;//工龄
        private TextView holdShare;//所占股份
        private TextView monthShare;//月收入
        //紧急联系人信息
        private TextView userName_1;//姓名
        private TextView phoneNumber_1;//电话
        private TextView rekation_1;//关系
        private TextView userName_2;
        private TextView phoneNumber_2;
        private TextView rekation_2;
        private TextView remark;//备注
    }
}
