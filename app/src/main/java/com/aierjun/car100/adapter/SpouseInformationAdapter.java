package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.SpouseInformationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class SpouseInformationAdapter extends BaseAdapter {
    private Context context;
    private List<SpouseInformationEntity> list;

    public SpouseInformationAdapter(Context context, List<SpouseInformationEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.spouseinformation_layout_item, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_buycarusername);
            viewHolder.sex = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_sex);
            viewHolder.age = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_age);
            viewHolder.cardNumber = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_cardNumber);
            viewHolder.cardAddress = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_cardAddress);
            viewHolder.educational = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_educational);
            viewHolder.censusRegister = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_censusRegister);
            viewHolder.phoneNumber = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_phoneNumber);
            viewHolder.addressNow = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_addressNow);
            viewHolder.telPhone = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_telPhone);
            viewHolder.unitType = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_unitType);
            viewHolder.dutyType = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_dutyType);
            viewHolder.duty = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_duty);
            viewHolder.runTime = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_runTime);
            viewHolder.monthShare = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_monthShare);
            viewHolder.remark = (TextView) convertView.findViewById(R.id.buycaruserinformation_item_remark);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        SpouseInformationEntity entity = (SpouseInformationEntity) getItem(position);
        viewHolder.name.setText(entity.getName());
        viewHolder.sex.setText(entity.getSex());
        viewHolder.age.setText(entity.getAge());
        viewHolder.cardNumber.setText(entity.getCardNumber());
        viewHolder.cardAddress.setText(entity.getCardAddress());
        viewHolder.educational.setText(entity.getEducational());
        viewHolder.censusRegister.setText(entity.getCensusRegister());
        viewHolder.phoneNumber.setText(entity.getPhoneNumber());
        viewHolder.addressNow.setText(entity.getAddressNow());
        viewHolder.telPhone.setText(entity.getTelPhone());
        viewHolder.unitType.setText(entity.getUnitType());
        viewHolder.dutyType.setText(entity.getDutyType());
        viewHolder.duty.setText(entity.getDuty());
        viewHolder.runTime.setText(entity.getRunTime());
        viewHolder.monthShare.setText(entity.getMonthShare() + "");
        viewHolder.remark.setText(entity.getRemark());
        return convertView;
    }

    class ViewHolder {
        private TextView name; //姓名
        private TextView sex; //性别
        private TextView age;//年龄
        private TextView cardNumber;//身份证号码
        private TextView cardAddress;//身份证归属地
        private TextView educational;//学历
        private TextView censusRegister;//户籍
        private TextView phoneNumber;//电话号码
        private TextView addressNow;//现居地
        private TextView telPhone;//固定电话
        private TextView unitType;//单位性质
        private TextView dutyType;//职务类型
        private TextView duty;//职务
        private TextView runTime;//经营期限
        private TextView monthShare;//月收入
        private TextView remark;//备注
    }
}
