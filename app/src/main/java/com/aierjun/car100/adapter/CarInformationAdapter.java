package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.CarInformationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class CarInformationAdapter extends BaseAdapter {
    private Context context;
    private List<CarInformationEntity> list;

    public CarInformationAdapter(Context context, List<CarInformationEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.carinformation_layout_item,null);
            viewHolder.carBigType = (TextView) convertView.findViewById(R.id.caruserinformation_item_carBigType);
            viewHolder.carType = (TextView) convertView.findViewById(R.id.caruserinformation_item_carType);
            viewHolder.seatNum = (TextView) convertView.findViewById(R.id.caruserinformation_item_seatNum);
            viewHolder.carNew = (TextView) convertView.findViewById(R.id.caruserinformation_item_carNew);
            viewHolder.publicTag = (TextView) convertView.findViewById(R.id.caruserinformation_item_publicTag);
            viewHolder.tagAddress = (TextView) convertView.findViewById(R.id.caruserinformation_item_tagAddress);
            viewHolder.drivingLicenseName = (TextView) convertView.findViewById(R.id.caruserinformation_item_drivingLicenseName);
            viewHolder.loansBankName = (TextView) convertView.findViewById(R.id.caruserinformation_item_loansBankName);
            viewHolder.loansTime = (TextView) convertView.findViewById(R.id.caruserinformation_item_loansTime);
            viewHolder.productType = (TextView) convertView.findViewById(R.id.caruserinformation_item_productType);
            viewHolder.productNumber = (TextView) convertView.findViewById(R.id.caruserinformation_item_productNumber);
            viewHolder.carMoney = (TextView) convertView.findViewById(R.id.caruserinformation_item_carMoney);
            viewHolder.loansMoney = (TextView) convertView.findViewById(R.id.caruserinformation_item_loansMoney);
            viewHolder.companyRate = (TextView) convertView.findViewById(R.id.caruserinformation_item_companyRate);
            viewHolder.firstPayment = (TextView) convertView.findViewById(R.id.caruserinformation_item_firstPayment);
            viewHolder.firstPaymentRatio = (TextView) convertView.findViewById(R.id.caruserinformation_item_firstPaymentRatio);
            viewHolder.loansRatio = (TextView) convertView.findViewById(R.id.caruserinformation_item_loansRatio);
            viewHolder.loansAllMoney = (TextView) convertView.findViewById(R.id.caruserinformation_item_loansAllMoney);
            viewHolder.loandStagesRatio = (TextView) convertView.findViewById(R.id.caruserinformation_item_loandStagesRatio);
            viewHolder.stagesMoney = (TextView) convertView.findViewById(R.id.caruserinformation_item_stagesMoney);
            viewHolder.stagesAllMoney = (TextView) convertView.findViewById(R.id.caruserinformation_item_stagesAllMoney);
            viewHolder.stagesMoneyRatio = (TextView) convertView.findViewById(R.id.caruserinformation_item_stagesMoneyRatio);
            viewHolder.monthRepayment = (TextView) convertView.findViewById(R.id.caruserinformation_item_monthRepayment);
            viewHolder.firstRepayment = (TextView) convertView.findViewById(R.id.caruserinformation_item_firstRepayment);
            viewHolder.remank = (TextView) convertView.findViewById(R.id.caruserinformation_item_remank);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CarInformationEntity entity = (CarInformationEntity) getItem(position);
        viewHolder.carBigType.setText(entity.getCarBigType());
        viewHolder.carType.setText(entity.getCarType());
        viewHolder.seatNum.setText(entity.getSeatNum() + "");
        viewHolder.carNew.setText(entity.getCarNew());
        viewHolder.publicTag.setText(entity.getPublicTag());
        viewHolder.tagAddress.setText(entity.getTagAddress());
        viewHolder.drivingLicenseName.setText(entity.getDrivingLicenseName());
        viewHolder.loansBankName.setText(entity.getLoansBankName());
        viewHolder.loansTime.setText(entity.getLoansTime());
        viewHolder.productType.setText(entity.getProductType());
        viewHolder.productNumber.setText(entity.getProductNumber());
        viewHolder.carMoney.setText(entity.getCarMoney() + "");
        viewHolder.loansMoney.setText(entity.getLoansMoney() + "");
        viewHolder.companyRate.setText(entity.getCompanyRate() + "");
        viewHolder.firstPayment.setText(entity.getFirstPayment() + "");
        viewHolder.firstPaymentRatio.setText(entity.getFirstPaymentRatio() + "");
        viewHolder.loansRatio.setText(entity.getLoansRatio() + "");
        viewHolder.loansAllMoney.setText(entity.getLoansAllMoney() + "");
        viewHolder.loandStagesRatio.setText(entity.getLoandStagesRatio() + "");
        viewHolder.stagesMoney.setText(entity.getStagesMoney() + "");
        viewHolder.stagesAllMoney.setText(entity.getStagesAllMoney() + "");
        viewHolder.stagesMoneyRatio.setText(entity.getStagesMoneyRatio() + "");
        viewHolder.monthRepayment.setText(entity.getMonthRepayment() + "");
        viewHolder.firstRepayment.setText(entity.getFirstRepayment() + "");
        viewHolder.remank.setText(entity.getRemank());
        return convertView;
    }
    
    class ViewHolder{
        private TextView carBigType;//车辆类型
        private TextView carType;//车型
        private TextView seatNum;//座位数
        private TextView carNew;//车型构成
        private TextView publicTag;//是否公牌
        private TextView tagAddress;//上牌地
        private TextView drivingLicenseName;//行驶证车主
        private TextView loansBankName;//贷款银行
        private TextView loansTime;//贷款年限
        private TextView productType;//产品类型
        private TextView productNumber;//产品编号
        private TextView carMoney;//车价
        private TextView loansMoney;//贷款额度
        private TextView companyRate;//公司费率
        private TextView firstPayment;//实际首付款
        private TextView firstPaymentRatio;//实际首付比例
        private TextView loansRatio;//实际贷款比例
        private TextView loansAllMoney;//分期付款总额
        private TextView loandStagesRatio;//分期付款比例
        private TextView stagesMoney;//分期手续费
        private TextView stagesAllMoney;//申请分期付款总额
        private TextView stagesMoneyRatio;//申请分期付款比例
        private TextView monthRepayment;//月还款
        private TextView firstRepayment;//首期还款
        private TextView remank;//备注
    }
}
