package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.CheckResultEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class CheckResultAdapter extends BaseAdapter {
    private Context context;
    private List<CheckResultEntity> list;

    public CheckResultAdapter(Context context, List<CheckResultEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.checkresult_layout_item,null);
            viewHolder.resultName = (TextView) convertView.findViewById(R.id.checkresult_item_resultName);
            viewHolder.checkUserName = (TextView) convertView.findViewById(R.id.checkresult_item_checkUserName);
            viewHolder.remank = (TextView) convertView.findViewById(R.id.checkresult_item_remank);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CheckResultEntity entity = (CheckResultEntity) getItem(position);
        viewHolder.resultName.setText(entity.getResultName());
        viewHolder.checkUserName.setText(entity.getCheckUserName());
        viewHolder.remank.setText(entity.getRemank());
        return convertView;
    }

    class ViewHolder{
        private TextView resultName; //结果名称
        private TextView checkUserName;//对象
        private TextView remank;//备注
    }
}
