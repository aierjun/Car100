package com.aierjun.car100.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aierjun.car100.R;
import com.aierjun.car100.entity.AfterLoaningInformationEntity;

import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class AfterLoaningInformationAdapter extends BaseAdapter {
    private Context context;
    private List<AfterLoaningInformationEntity> list;

    public AfterLoaningInformationAdapter(Context context, List<AfterLoaningInformationEntity> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.afterloaninginformation_layout_item,null);
            viewHolder.cardID = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_cardID);
            viewHolder.cardSendAddress = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_cardSendAddress);
            viewHolder.cardSendTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_cardSendTime);
            viewHolder.cardAcceptTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_cardAcceptTime);
            viewHolder.companyGiveTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_companyGiveTime);
            viewHolder.contractSendBankTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_contractSendBankTime);
            viewHolder.carReachTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_carReachTime);
            viewHolder.registerCredentialReachTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_registerCredentialReachTime);
            viewHolder.bankGiveTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_bankGiveTime);
            viewHolder.sendInformationTime = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_sendInformationTime);
            viewHolder.pledgeTransaction = (TextView) convertView.findViewById(R.id.afterloaninginformation_item_pledgeTransaction);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        AfterLoaningInformationEntity entity = (AfterLoaningInformationEntity) getItem(position);
        viewHolder.cardID.setText(entity.getCardID());
        viewHolder.cardSendAddress.setText(entity.getCardSendAddress());
        viewHolder.cardSendTime.setText(entity.getCardSendTime());
        viewHolder.cardAcceptTime.setText(entity.getCardAcceptTime());
        viewHolder.companyGiveTime.setText(entity.getCompanyGiveTime());
        viewHolder.contractSendBankTime.setText(entity.getContractSendBankTime());
        viewHolder.carReachTime.setText(entity.getCarReachTime());
        viewHolder.registerCredentialReachTime.setText(entity.getRegisterCredentialReachTime());
        viewHolder.bankGiveTime.setText(entity.getBankGiveTime());
        viewHolder.sendInformationTime.setText(entity.getSendInformationTime());
        viewHolder.pledgeTransaction.setText(entity.getPledgeTransaction());
        return convertView;
    }
    
    class ViewHolder{
        private TextView cardID;//贷记卡号
        private TextView cardSendAddress;//卡片寄送地址
        private TextView cardSendTime;//还款卡片寄出日期
        private TextView cardAcceptTime;//还款卡片收到日期
        private TextView companyGiveTime;//公司垫付日期
        private TextView contractSendBankTime;//合同送银行日期
        private TextView carReachTime;//车资到达日期
        private TextView registerCredentialReachTime;//登记证书到达日期
        private TextView bankGiveTime;//银行放贷日期
        private TextView sendInformationTime;//抵押资料寄网点日期
        private TextView pledgeTransaction;//抵押办理
    }
}
