package com.aierjun.car100.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2017/9/12.
 */

public class MySharedPreferences {

    public static void saveStringList(Context context,String shareName , List<String> name, List<String> data) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(shareName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (int i = 0; i < name.size(); i++)
            editor.putString(name.get(i), data.get(i));
        editor.commit();
        Toast.makeText(context,"数据已保存",Toast.LENGTH_SHORT).show();
    }

    public static  List<String> getStringList(Context context,String shareName , List<String> name) {
        List<String> list = new ArrayList<>();
        SharedPreferences sharedPreferences = context.getSharedPreferences(shareName, Context.MODE_PRIVATE);
        for (int i = 0; i < name.size(); i++){
            list.add(sharedPreferences.getString(name.get(i), ""));
        }
        return list;
    }
}
