package com.aierjun.car100.utils;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class DateTimeUtils {
    private static SimpleDateFormat dateFormat;

    public static String getFormatDateTime(long datetime) {
        if (dateFormat == null)
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(datetime);
    }
}
