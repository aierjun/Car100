package com.aierjun.car100.utils;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Environment;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.File;

/**
 * Created by Ani_aierJun on 2017/9/6.
 */

public class MediaRecorderUtils {
    private static boolean isRecording = false;

    public static SurfaceView setSurfaceView(SurfaceView surfaceView) {
        surfaceView.getHolder().setFixedSize(1280, 720);
        // 设置该组件让屏幕不会自动关闭
        surfaceView.getHolder().setKeepScreenOn(true);
        return surfaceView;
    }

    public static void startVideo(Context context,SurfaceView surfaceView,MediaRecorder mRecorder) {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(context
                    , "SD卡不存在，请插入SD卡！"
                    , Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            // 创建保存录制视频的视频文件
            File videoFile = new File(Environment
                    .getExternalStorageDirectory()
                    .getCanonicalFile() + "/testvideo.3gp");
            // 创建MediaPlayer对象
            mRecorder = new MediaRecorder();
            mRecorder.reset();
            // 设置从麦克风采集声音(或来自录像机的声音AudioSource.CAMCORDER)
            mRecorder.setAudioSource(MediaRecorder
                    .AudioSource.MIC);
            // 设置从摄像头采集图像
            mRecorder.setVideoSource(MediaRecorder
                    .VideoSource.CAMERA);
            // 设置视频文件的输出格式
            // 必须在设置声音编码格式、图像编码格式之前设置
            mRecorder.setOutputFormat(MediaRecorder
                    .OutputFormat.THREE_GPP);
            // 设置声音编码的格式
            mRecorder.setAudioEncoder(MediaRecorder
                    .AudioEncoder.AMR_NB);
            // 设置图像编码的格式
            mRecorder.setVideoEncoder(MediaRecorder
                    .VideoEncoder.H264);
            mRecorder.setVideoSize(1280, 720);
            // 每秒 4帧
            mRecorder.setVideoFrameRate(20);
            mRecorder.setOutputFile(videoFile.getAbsolutePath());
            // 指定使用SurfaceView来预览视频
            mRecorder.setPreviewDisplay(surfaceView
                    .getHolder().getSurface());  //①
            mRecorder.prepare();
            // 开始录制
            mRecorder.start();
            System.out.println("---recording---");
            // 让record按钮不可用。
//            record.setEnabled(false);
//            // 让stop按钮可用。
//            stop.setEnabled(true);
            isRecording = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stopVideo(MediaRecorder mRecorder){
        // 如果正在进行录制
        if (isRecording) {
            // 停止录制
            mRecorder.stop();
            // 释放资源
            mRecorder.release();
            mRecorder = null;
            // 让record按钮可用。
//          record.setEnabled(true);
//          让stop按钮不可用。
//          stop.setEnabled(false);
        }
    }
}
