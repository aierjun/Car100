package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */

public class DoingBusinessEntity {
    private int state; //到达阶段状态
    private long time; //提交时间
    private String userNumber; //客户编号
    private String userName;//客户名


    public DoingBusinessEntity() {
    }

    public DoingBusinessEntity(int state, long time, String userNumber, String userName) {
        this.state = state;
        this.time = time;
        this.userNumber = userNumber;
        this.userName = userName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
