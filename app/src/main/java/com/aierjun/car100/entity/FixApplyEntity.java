package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class FixApplyEntity {
    private String time;//时间
    private String state;//状态
    private String userNumber;//客户编号
    private String userName;//客户名

    public FixApplyEntity() {
    }

    public FixApplyEntity(String time, String state, String userNumber, String userName) {
        this.time = time;
        this.state = state;
        this.userNumber = userNumber;
        this.userName = userName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
