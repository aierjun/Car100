package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/14.
 */

public class NotifacationEntity {
    private String state;
    private String time;
    private String number;
    private String name;

    public NotifacationEntity() {
    }

    public NotifacationEntity(String state, String time, String number, String name) {
        this.state = state;
        this.time = time;
        this.number = number;
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
