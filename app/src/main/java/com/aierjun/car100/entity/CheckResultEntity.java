package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class CheckResultEntity {
    private String resultName; //结果名称
    private String checkUserName;//对象
    private String remank;//备注

    public CheckResultEntity() {
    }

    public CheckResultEntity(String resultName, String checkUserName, String remank) {
        this.resultName = resultName;
        this.checkUserName = checkUserName;
        this.remank = remank;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public String getCheckUserName() {
        return checkUserName;
    }

    public void setCheckUserName(String checkUserName) {
        this.checkUserName = checkUserName;
    }

    public String getRemank() {
        return remank;
    }

    public void setRemank(String remank) {
        this.remank = remank;
    }
}
