package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class AfterLoaningInformationEntity {
    private String cardID;//贷记卡号
    private String cardSendAddress;//卡片寄送地址
    private String cardSendTime;//还款卡片寄出日期
    private String cardAcceptTime;//还款卡片收到日期
    private String companyGiveTime;//公司垫付日期
    private String contractSendBankTime;//合同送银行日期
    private String carReachTime;//车资到达日期
    private String registerCredentialReachTime;//登记证书到达日期
    private String bankGiveTime;//银行放贷日期
    private String sendInformationTime;//抵押资料寄网点日期
    private String pledgeTransaction;//抵押办理

    public AfterLoaningInformationEntity() {
    }

    public AfterLoaningInformationEntity(String cardID, String cardSendAddress, String cardSendTime, String cardAcceptTime, String companyGiveTime, String contractSendBankTime, String carReachTime, String registerCredentialReachTime, String bankGiveTime, String sendInformationTime, String pledgeTransaction) {
        this.cardID = cardID;
        this.cardSendAddress = cardSendAddress;
        this.cardSendTime = cardSendTime;
        this.cardAcceptTime = cardAcceptTime;
        this.companyGiveTime = companyGiveTime;
        this.contractSendBankTime = contractSendBankTime;
        this.carReachTime = carReachTime;
        this.registerCredentialReachTime = registerCredentialReachTime;
        this.bankGiveTime = bankGiveTime;
        this.sendInformationTime = sendInformationTime;
        this.pledgeTransaction = pledgeTransaction;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getCardSendAddress() {
        return cardSendAddress;
    }

    public void setCardSendAddress(String cardSendAddress) {
        this.cardSendAddress = cardSendAddress;
    }

    public String getCardSendTime() {
        return cardSendTime;
    }

    public void setCardSendTime(String cardSendTime) {
        this.cardSendTime = cardSendTime;
    }

    public String getCardAcceptTime() {
        return cardAcceptTime;
    }

    public void setCardAcceptTime(String cardAcceptTime) {
        this.cardAcceptTime = cardAcceptTime;
    }

    public String getCompanyGiveTime() {
        return companyGiveTime;
    }

    public void setCompanyGiveTime(String companyGiveTime) {
        this.companyGiveTime = companyGiveTime;
    }

    public String getContractSendBankTime() {
        return contractSendBankTime;
    }

    public void setContractSendBankTime(String contractSendBankTime) {
        this.contractSendBankTime = contractSendBankTime;
    }

    public String getCarReachTime() {
        return carReachTime;
    }

    public void setCarReachTime(String carReachTime) {
        this.carReachTime = carReachTime;
    }

    public String getRegisterCredentialReachTime() {
        return registerCredentialReachTime;
    }

    public void setRegisterCredentialReachTime(String registerCredentialReachTime) {
        this.registerCredentialReachTime = registerCredentialReachTime;
    }

    public String getBankGiveTime() {
        return bankGiveTime;
    }

    public void setBankGiveTime(String bankGiveTime) {
        this.bankGiveTime = bankGiveTime;
    }

    public String getSendInformationTime() {
        return sendInformationTime;
    }

    public void setSendInformationTime(String sendInformationTime) {
        this.sendInformationTime = sendInformationTime;
    }

    public String getPledgeTransaction() {
        return pledgeTransaction;
    }

    public void setPledgeTransaction(String pledgeTransaction) {
        this.pledgeTransaction = pledgeTransaction;
    }
}
