package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */
/*
* 购车人信息
* */

public class BuyCarUserInformationEntity {
    private String name; //姓名
    private String sex; //性别
    private String age;//年龄
    private String cardNumber;//身份证号码
    private String cardAddress;//身份证归属地
    private String educational;//学历
    private String censusRegister;//户籍
    private String phoneNumber;//电话号码
    private String marry;//婚否
    private String addressNow;//现居地
    private String telPhone;//固定电话
    private String homeAddress;//实际家庭住址
    private String homeRightAddress;//房产地址
    private String homeRightUserName;//房产所有权人
    private String bugCarRelation;//与购车人关系
    private String homeType;//房产性质
    private float homeSize;//房屋面积
    private float homeMoney;//目前市价
    private float homeLoansMoney;//房贷金额
    private int ageLimit;//年限
    private float monthRefund;//月还款
    private String unitType;//单位性质
    private String dutyType;//职务类型
    private String duty;//职务
    private String runTime;//经营期限
    private String workTime;//工龄
    private float holdShare;//所占股份
    private float monthShare;//月收入
    //紧急联系人信息
    private String userName_1;//姓名
    private String phoneNumber_1;//电话
    private String rekation_1;//关系
    private String userName_2;
    private String phoneNumber_2;
    private String rekation_2;
    private String remark;//备注

    public BuyCarUserInformationEntity() {
    }

    public BuyCarUserInformationEntity(String name, String sex, String age, String cardNumber, String cardAddress, String educational, String censusRegister, String phoneNumber, String marry, String addressNow, String telPhone, String homeAddress, String homeRightAddress, String homeRightUserName, String bugCarRelation, String homeType, float homeSize, float homeMoney, float homeLoansMoney, int ageLimit, float monthRefund, String unitType, String dutyType, String duty, String runTime, String workTime, float holdShare, float monthShare, String userName_1, String phoneNumber_1, String rekation_1, String userName_2, String phoneNumber_2, String rekation_2, String remark) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.cardNumber = cardNumber;
        this.cardAddress = cardAddress;
        this.educational = educational;
        this.censusRegister = censusRegister;
        this.phoneNumber = phoneNumber;
        this.marry = marry;
        this.addressNow = addressNow;
        this.telPhone = telPhone;
        this.homeAddress = homeAddress;
        this.homeRightAddress = homeRightAddress;
        this.homeRightUserName = homeRightUserName;
        this.bugCarRelation = bugCarRelation;
        this.homeType = homeType;
        this.homeSize = homeSize;
        this.homeMoney = homeMoney;
        this.homeLoansMoney = homeLoansMoney;
        this.ageLimit = ageLimit;
        this.monthRefund = monthRefund;
        this.unitType = unitType;
        this.dutyType = dutyType;
        this.duty = duty;
        this.runTime = runTime;
        this.workTime = workTime;
        this.holdShare = holdShare;
        this.monthShare = monthShare;
        this.userName_1 = userName_1;
        this.phoneNumber_1 = phoneNumber_1;
        this.rekation_1 = rekation_1;
        this.userName_2 = userName_2;
        this.phoneNumber_2 = phoneNumber_2;
        this.rekation_2 = rekation_2;
        this.remark = remark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardAddress() {
        return cardAddress;
    }

    public void setCardAddress(String cardAddress) {
        this.cardAddress = cardAddress;
    }

    public String getEducational() {
        return educational;
    }

    public void setEducational(String educational) {
        this.educational = educational;
    }

    public String getCensusRegister() {
        return censusRegister;
    }

    public void setCensusRegister(String censusRegister) {
        this.censusRegister = censusRegister;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMarry() {
        return marry;
    }

    public void setMarry(String marry) {
        this.marry = marry;
    }

    public String getAddressNow() {
        return addressNow;
    }

    public void setAddressNow(String addressNow) {
        this.addressNow = addressNow;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getHomeRightAddress() {
        return homeRightAddress;
    }

    public void setHomeRightAddress(String homeRightAddress) {
        this.homeRightAddress = homeRightAddress;
    }

    public String getHomeRightUserName() {
        return homeRightUserName;
    }

    public void setHomeRightUserName(String homeRightUserName) {
        this.homeRightUserName = homeRightUserName;
    }

    public String getBugCarRelation() {
        return bugCarRelation;
    }

    public void setBugCarRelation(String bugCarRelation) {
        this.bugCarRelation = bugCarRelation;
    }

    public String getHomeType() {
        return homeType;
    }

    public void setHomeType(String homeType) {
        this.homeType = homeType;
    }

    public float getHomeSize() {
        return homeSize;
    }

    public void setHomeSize(float homeSize) {
        this.homeSize = homeSize;
    }

    public float getHomeMoney() {
        return homeMoney;
    }

    public void setHomeMoney(float homeMoney) {
        this.homeMoney = homeMoney;
    }

    public float getHomeLoansMoney() {
        return homeLoansMoney;
    }

    public void setHomeLoansMoney(float homeLoansMoney) {
        this.homeLoansMoney = homeLoansMoney;
    }

    public int getAgeLimit() {
        return ageLimit;
    }

    public void setAgeLimit(int ageLimit) {
        this.ageLimit = ageLimit;
    }

    public float getMonthRefund() {
        return monthRefund;
    }

    public void setMonthRefund(float monthRefund) {
        this.monthRefund = monthRefund;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getDutyType() {
        return dutyType;
    }

    public void setDutyType(String dutyType) {
        this.dutyType = dutyType;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public float getHoldShare() {
        return holdShare;
    }

    public void setHoldShare(float holdShare) {
        this.holdShare = holdShare;
    }

    public float getMonthShare() {
        return monthShare;
    }

    public void setMonthShare(float monthShare) {
        this.monthShare = monthShare;
    }

    public String getUserName_1() {
        return userName_1;
    }

    public void setUserName_1(String userName_1) {
        this.userName_1 = userName_1;
    }

    public String getPhoneNumber_1() {
        return phoneNumber_1;
    }

    public void setPhoneNumber_1(String phoneNumber_1) {
        this.phoneNumber_1 = phoneNumber_1;
    }

    public String getRekation_1() {
        return rekation_1;
    }

    public void setRekation_1(String rekation_1) {
        this.rekation_1 = rekation_1;
    }

    public String getUserName_2() {
        return userName_2;
    }

    public void setUserName_2(String userName_2) {
        this.userName_2 = userName_2;
    }

    public String getPhoneNumber_2() {
        return phoneNumber_2;
    }

    public void setPhoneNumber_2(String phoneNumber_2) {
        this.phoneNumber_2 = phoneNumber_2;
    }

    public String getRekation_2() {
        return rekation_2;
    }

    public void setRekation_2(String rekation_2) {
        this.rekation_2 = rekation_2;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
