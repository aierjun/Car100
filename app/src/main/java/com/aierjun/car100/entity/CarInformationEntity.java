package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class CarInformationEntity {
    private String carBigType;//车辆类型
    private String carType;//车型
    private int seatNum;//座位数
    private String carNew;//车型构成
    private String publicTag;//是否公牌
    private String tagAddress;//上牌地
    private String drivingLicenseName;//行驶证车主
    private String loansBankName;//贷款银行
    private String loansTime;//贷款年限
    private String productType;//产品类型
    private String productNumber;//产品编号
    private float carMoney;//车价
    private float loansMoney;//贷款额度
    private float companyRate;//公司费率
    private float firstPayment;//实际首付款
    private float firstPaymentRatio;//实际首付比例
    private float loansRatio;//实际贷款比例
    private float loansAllMoney;//分期付款总额
    private float loandStagesRatio;//分期付款比例
    private float stagesMoney;//分期手续费
    private float stagesAllMoney;//申请分期付款总额
    private float stagesMoneyRatio;//申请分期付款比例
    private float monthRepayment;//月还款
    private float firstRepayment;//首期还款
    private String remank;//备注

    public CarInformationEntity() {
    }

    public CarInformationEntity(String carBigType, String carType, int seatNum, String carNew, String publicTag, String tagAddress, String drivingLicenseName, String loansBankName, String loansTime, String productType, String productNumber, float carMoney, float loansMoney, float companyRate, float firstPayment, float firstPaymentRatio, float loansRatio, float loansAllMoney, float loandStagesRatio, float stagesMoney, float stagesAllMoney, float stagesMoneyRatio, float monthRepayment, float firstRepayment, String remank) {
        this.carBigType = carBigType;
        this.carType = carType;
        this.seatNum = seatNum;
        this.carNew = carNew;
        this.publicTag = publicTag;
        this.tagAddress = tagAddress;
        this.drivingLicenseName = drivingLicenseName;
        this.loansBankName = loansBankName;
        this.loansTime = loansTime;
        this.productType = productType;
        this.productNumber = productNumber;
        this.carMoney = carMoney;
        this.loansMoney = loansMoney;
        this.companyRate = companyRate;
        this.firstPayment = firstPayment;
        this.firstPaymentRatio = firstPaymentRatio;
        this.loansRatio = loansRatio;
        this.loansAllMoney = loansAllMoney;
        this.loandStagesRatio = loandStagesRatio;
        this.stagesMoney = stagesMoney;
        this.stagesAllMoney = stagesAllMoney;
        this.stagesMoneyRatio = stagesMoneyRatio;
        this.monthRepayment = monthRepayment;
        this.firstRepayment = firstRepayment;
        this.remank = remank;
    }

    public String getCarBigType() {
        return carBigType;
    }

    public void setCarBigType(String carBigType) {
        this.carBigType = carBigType;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public int getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(int seatNum) {
        this.seatNum = seatNum;
    }

    public String getCarNew() {
        return carNew;
    }

    public void setCarNew(String carNew) {
        this.carNew = carNew;
    }

    public String getPublicTag() {
        return publicTag;
    }

    public void setPublicTag(String publicTag) {
        this.publicTag = publicTag;
    }

    public String getTagAddress() {
        return tagAddress;
    }

    public void setTagAddress(String tagAddress) {
        this.tagAddress = tagAddress;
    }

    public String getDrivingLicenseName() {
        return drivingLicenseName;
    }

    public void setDrivingLicenseName(String drivingLicenseName) {
        this.drivingLicenseName = drivingLicenseName;
    }

    public String getLoansBankName() {
        return loansBankName;
    }

    public void setLoansBankName(String loansBankName) {
        this.loansBankName = loansBankName;
    }

    public String getLoansTime() {
        return loansTime;
    }

    public void setLoansTime(String loansTime) {
        this.loansTime = loansTime;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public float getCarMoney() {
        return carMoney;
    }

    public void setCarMoney(float carMoney) {
        this.carMoney = carMoney;
    }

    public float getLoansMoney() {
        return loansMoney;
    }

    public void setLoansMoney(float loansMoney) {
        this.loansMoney = loansMoney;
    }

    public float getCompanyRate() {
        return companyRate;
    }

    public void setCompanyRate(float companyRate) {
        this.companyRate = companyRate;
    }

    public float getFirstPayment() {
        return firstPayment;
    }

    public void setFirstPayment(float firstPayment) {
        this.firstPayment = firstPayment;
    }

    public float getFirstPaymentRatio() {
        return firstPaymentRatio;
    }

    public void setFirstPaymentRatio(float firstPaymentRatio) {
        this.firstPaymentRatio = firstPaymentRatio;
    }

    public float getLoansRatio() {
        return loansRatio;
    }

    public void setLoansRatio(float loansRatio) {
        this.loansRatio = loansRatio;
    }

    public float getLoansAllMoney() {
        return loansAllMoney;
    }

    public void setLoansAllMoney(float loansAllMoney) {
        this.loansAllMoney = loansAllMoney;
    }

    public float getLoandStagesRatio() {
        return loandStagesRatio;
    }

    public void setLoandStagesRatio(float loandStagesRatio) {
        this.loandStagesRatio = loandStagesRatio;
    }

    public float getStagesMoney() {
        return stagesMoney;
    }

    public void setStagesMoney(float stagesMoney) {
        this.stagesMoney = stagesMoney;
    }

    public float getStagesAllMoney() {
        return stagesAllMoney;
    }

    public void setStagesAllMoney(float stagesAllMoney) {
        this.stagesAllMoney = stagesAllMoney;
    }

    public float getStagesMoneyRatio() {
        return stagesMoneyRatio;
    }

    public void setStagesMoneyRatio(float stagesMoneyRatio) {
        this.stagesMoneyRatio = stagesMoneyRatio;
    }

    public float getMonthRepayment() {
        return monthRepayment;
    }

    public void setMonthRepayment(float monthRepayment) {
        this.monthRepayment = monthRepayment;
    }

    public float getFirstRepayment() {
        return firstRepayment;
    }

    public void setFirstRepayment(float firstRepayment) {
        this.firstRepayment = firstRepayment;
    }

    public String getRemank() {
        return remank;
    }

    public void setRemank(String remank) {
        this.remank = remank;
    }
}
