package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/21.
 */

public class AccommodateApplyEntity {
    private String callbackTime;// 退回时间
    private String state;//状态
    private String orderNumber;//订单号
    private String userName;//客户名
    private String callbackBeginState;//退回前状态
    private String checkName;//审核人
    private String callbackReason;//退回原因
    private String s;

    public AccommodateApplyEntity() {
    }

    public AccommodateApplyEntity(String callbackTime, String state, String orderNumber, String userName, String callbackBeginState, String checkName, String callbackReason, String s) {
        this.callbackTime = callbackTime;
        this.state = state;
        this.orderNumber = orderNumber;
        this.userName = userName;
        this.callbackBeginState = callbackBeginState;
        this.checkName = checkName;
        this.callbackReason = callbackReason;
        this.s = s;
    }

    public String getCallbackTime() {
        return callbackTime;
    }

    public void setCallbackTime(String callbackTime) {
        this.callbackTime = callbackTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCallbackBeginState() {
        return callbackBeginState;
    }

    public void setCallbackBeginState(String callbackBeginState) {
        this.callbackBeginState = callbackBeginState;
    }

    public String getCheckName() {
        return checkName;
    }

    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    public String getCallbackReason() {
        return callbackReason;
    }

    public void setCallbackReason(String callbackReason) {
        this.callbackReason = callbackReason;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }
}
