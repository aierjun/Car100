package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */
/*
* 基本信息
* */

public class BasicInformationEntity {
    private String userName; //业务来源人
    private String userNumber; //客户编号
    private String signOperator; //签约营销商
    private String operator; //营销商

    public BasicInformationEntity() {
    }

    public BasicInformationEntity(String userName, String userNumber, String signOperator, String operator) {
        this.userName = userName;
        this.userNumber = userNumber;
        this.signOperator = signOperator;
        this.operator = operator;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getSignOperator() {
        return signOperator;
    }

    public void setSignOperator(String signOperator) {
        this.signOperator = signOperator;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
