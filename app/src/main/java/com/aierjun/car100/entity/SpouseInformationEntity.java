package com.aierjun.car100.entity;

/**
 * Created by Ani_aierJun on 2017/9/20.
 */
/*
* 配偶信息
* */

public class SpouseInformationEntity {
    private String name; //姓名
    private String sex; //性别
    private String age;//年龄
    private String cardNumber;//身份证号码
    private String cardAddress;//身份证归属地
    private String educational;//学历
    private String censusRegister;//户籍
    private String phoneNumber;//电话号码
    private String addressNow;//现居地
    private String telPhone;//固定电话
    private String unitType;//单位性质
    private String dutyType;//职务类型
    private String duty;//职务
    private String runTime;//经营期限
    private float monthShare;//月收入
    private String remark;//备注

    public SpouseInformationEntity() {
    }

    public SpouseInformationEntity(String name, String sex, String age, String cardNumber, String cardAddress, String educational, String censusRegister, String phoneNumber, String addressNow, String telPhone, String unitType, String dutyType, String duty, String runTime, float monthShare, String remark) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.cardNumber = cardNumber;
        this.cardAddress = cardAddress;
        this.educational = educational;
        this.censusRegister = censusRegister;
        this.phoneNumber = phoneNumber;
        this.addressNow = addressNow;
        this.telPhone = telPhone;
        this.unitType = unitType;
        this.dutyType = dutyType;
        this.duty = duty;
        this.runTime = runTime;
        this.monthShare = monthShare;
        this.remark = remark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardAddress() {
        return cardAddress;
    }

    public void setCardAddress(String cardAddress) {
        this.cardAddress = cardAddress;
    }

    public String getEducational() {
        return educational;
    }

    public void setEducational(String educational) {
        this.educational = educational;
    }

    public String getCensusRegister() {
        return censusRegister;
    }

    public void setCensusRegister(String censusRegister) {
        this.censusRegister = censusRegister;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddressNow() {
        return addressNow;
    }

    public void setAddressNow(String addressNow) {
        this.addressNow = addressNow;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getDutyType() {
        return dutyType;
    }

    public void setDutyType(String dutyType) {
        this.dutyType = dutyType;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public float getMonthShare() {
        return monthShare;
    }

    public void setMonthShare(float monthShare) {
        this.monthShare = monthShare;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
